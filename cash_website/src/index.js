import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';

import {postRequest} from './universal_method/postRequest';
import {MainContent} from './component/MainContent';
import {Navigation} from './component/Navigation';
class WM extends React.Component{
  constructor(props) {
    super(props);
    this.state={
		currentMainContent:"Home",
		currentNav:"Classic",
		account:'',
		password:''	,
		sid:'',		
		category:{
			promotions:{
				name:"Promotions"
			},
			register:
			{
				name:"Register",
				detectSymbol:/[-~`!@#$%^&()_+{}?;:/><,*=\s]/,
				preventAlert:false,
				currency:'RMB',
				register_account:'',
				register_password:'',
				realName:'',
				repeatPS:'',
				agent:'',			
				email:'',
				phoneNumber:'',
				QQ:'',
				WC:''	
			},
			accountInfo:{
				name:"Account",
				account:'',
				username:'',
				balance:'',
				phone:'',
				currency:'',
				lastLogin:'',
				registerTime:''		
			},
			download:{
				name:"Download"
			},
			deposit:{
				name:"Deposit"
			},
			withdraw:{
				name:"Withdraw"
			},			
			gameHistory:{
				name:"GameHistory"
			},
			financialHistory:{
				name:"FinancialHistory"
			},
			about:{
				name:"About",
				currentMainContent:"About"
			}
		},
		mobileNav:false
	};
	this.initiate();
  }	
//----------------handle-------------------------		
	handleBlur(event){//離開該欄位時觸發的判斷
		const target=event.target;
		// console.log(target.name);
		switch (target.name){		
			case 'realName':
				break;
			case 'register_account':
				break;
			case 'register_password':
				break;
			case 'repeatPS'://輸入完換focus如果不同會focus回出入欄並清除
				if(target.value!==this.state.category.register.register_password && this.state.category.register.preventAlert){
					let tempState = this.state.category;
					tempState.register.preventAlert=false;
					tempState.register.repeatPS="";
					this.setState({
						category:tempState
					})
					alert("必須與密碼一致");					
					target.focus();					
				}
				break;
			case 'agent':
				break;
			default:
				break;																			
		}
		window.bRotate=!window.bRotate;
	}	
	handleChange(event){//每一鍵輸入後觸發 檢查輸入字元
		const target=event.target;
		if(this.state.category.register.detectSymbol.test(target.value)){
			alert("禁止包含特殊字元");					
			target.focus();
			return;
		}
		switch (target.name){		
			case 'realName':
				if(target.value.length > 30){
					alert("姓名不能超過30字元");					
					target.focus();					
					return;
				}
				break;
			case 'register_account':
				if(target.value.length > 17){
					alert("帳號不能超過17字元");					
					target.focus();							
					return;
				}
				break;
			case 'register_password':
				if(target.value.length > 20){
					alert("密碼不能超過20字元");					
					target.focus();						
					return;
				}			
				break;
			case 'repeatPS':
				let tempState = this.state.category;
				tempState.register.preventAlert=true;
				this.setState({
					category:tempState
				})
				if(target.value.length > 20){
					alert("密碼不能超過20字元");					
					target.focus();							
					return;
				}			
				break;
			case 'agent':	
				break;
			default:
				break;																			
		}
		if(target.name==="account" || target.name==="password"){	
			this.setState({
				[target.name]: target.value
			});
		}else{
			let tempState = this.state.category;
			tempState.register[target.name]=target.value;
			this.setState({
				category:tempState
			})
		}				
	}
	
//----------------submit-------------------------			
	submitRegister(event){
		event.preventDefault();
		let url =  'https://api-01.a45.me/api/web/Gateway.php';
        let request = {
            cmd: 'MemberRegister',
			promotecode: this.state.category.register.agent,
			username: this.state.category.register.register_account,
			password: this.state.category.register.register_password,
			name: this.state.category.register.realName,
			currency: 'RMB'
		};
		postRequest(url, request ,(response) => this.registerFunc(response));
	}
	submitLogin(event){
		event.preventDefault();
		let url =  'https://api-01.a45.me/api/web/Gateway.php';
        let request = {
            cmd: 'GetSid',
            user:this.state.account,
            password:this.state.password
		};		
		postRequest(url, request ,(response) => this.getSidFunc(response));
		this.setState({
			account:'',
			password:''
		});
	}	
//----------------cookie-----------------------------
	setCookie(cname,cvalue,exdays){
		if(exdays){
		    let d = new Date();
		    d.setTime(d.getTime()+(exdays*24*60*60*1000));
		    // d.setTime(d.getTime()+(exdays*10*1000));

		    let expires = "expires="+d.toGMTString();
		    document.cookie = cname+"="+cvalue+"; "+expires;			
		}else{
			document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
		}
	}
	getCookie(cname){
	    let name = cname + "=";
	    let ca = document.cookie.split(';');
	    for(let i=0; i<ca.length; i++) {
	        let c = ca[i].trim();
	        if (c.indexOf(name)===0) { return c.substring(name.length,c.length); }
	    }
	    return "";
	}
	// checkCookie(){
	//     let user=this.getCookie("username");
	//     if (user!=""){
	//         alert("欢迎 " + user + " 再次访问");
	//     }
	//     else {
	//         user = prompt("请输入你的名字:","");
	//           if (user!="" && user!=null){
	//             this.setCookie("username",user,30);
	//         }
	//     }
	// }
//----------------API_callback-----------------------	
	registerFunc(response){
		if(response.errorCode===0){
			let tempState=this.state.category;
			tempState.register={
				name:"Register",
				detectSymbol:/[-~`!@#$%^&()_+{}?;:/><,*=\s]/,				
				preventAlert:false,
				currency:'RMB',
				realName:'',
				register_account:'',
				repeatPS:'',
				register_password:'',
				agent:'',
				email:'',
				phoneNumber:'',
				QQ:'',
				WC:''
			}
			this.setState({
				category:tempState
			});
			alert("注册成功");
			console.log(response)
			let url =  'https://api-01.a45.me/api/web/Gateway.php';
	        let request = {
	            cmd: 'Login',
	            sid: response.result.sid
			};
			this.setState({
				sid:response.result.sid
			})//註冊成功後會直接幫你做登入
			postRequest(url, request ,(response) => this.signInFunc(response));
		}else{
			alert("是在哈囉");
		}
	}		
	signInFunc(response,sid,repeat){
		if(response.errorCode===0){
			console.log(response);
			let tempState = this.state.category;
			tempState.accountInfo={
				name:"Account",
				account:response.result.user,
				username:response.result.username,
				balance:response.result.cash,
				phone:response.result.phone,
				currency:response.result.currency,
				lastLogin:response.result.lastLogin,
				registerTime:response.result.registerTime		
			}
			this.setState({
				category:tempState
			})
			if(!repeat)
			this.chooseMainContent('Home');
			console.log("SSSSS="+sid)
			this.setState({
				sid:sid
			})

			setTimeout(this.keepLogin.bind(this),15000)			
		}else{
			alert("您已被登出,请重新登入");
			this.logOut();
		}
	}
	keepLogin(sid){
		if(this.state.sid===""){
			return
		}
		let url =  'https://api-01.a45.me/api/web/Gateway.php';
        let request = {
            cmd: 'Login',
            sid: this.state.sid
		};			
		postRequest(url, request ,(response) => this.signInFunc(response,this.state.sid,true));			
	}
	getSidFunc(response){
		if(response.errorCode===0){
			let url =  'https://api-01.a45.me/api/web/Gateway.php';
			let sid=response.result.sid;
	        let request = {
	            cmd: 'Login',
	            sid: sid
			};
			console.log("give me "+sid);
			this.setCookie("sid",sid,3);						
			postRequest(url, request ,(response) => this.signInFunc(response,sid));
		}else{
			alert("wrong account or password");
		}		
	}
	openGameWindow(response){
		if(response.errorCode===0){
			console.log(response.result);
			setTimeout(function(){window.open(response.result)}, 500);
			// window.open(response.result)
		}else{
			alert("wrong openGameWindow");
		}	
	}
	prepareOpenGame(){//用sid去取得打開wm遊戲網址
		let url =  'https://api-01.a45.me/api/web/Gateway.php';
	    let request = {
	        cmd: 'OpenGame',
	        sid: this.state.sid,
	        ui:'0'
		};
		postRequest(url, request ,(response) => this.openGameWindow(response));		
	}
	openSportWindow(response){
		if(response.errorCode===0){
			console.log(response.result);
			setTimeout(function(){window.open(response.result.url)}, 500);
			
		}else{
			alert("wrong openSportWindow");
		}			
	}
	prepareSportGame(){
		let url = 'https://api3rd.a45.me/api/Gateway.php';
	    let request = {
	        action: 'login',
	        sid: this.state.sid,
	        mobile:window.bMobile?'Y':'N',
	        code:"5002",
	        currency:this.state.category.accountInfo.currency,
	        lang:"cn"
		};
		console.log(request);
		postRequest(url, request ,(response) => this.openSportWindow(response));		
	}	

//-----------------choose---------------	
	chooseMainContent(category){
		// console.log("chooseMainContent");
		this.setState({
			currentMainContent:category
		});
	}
	chooseSubContent(category){
		let tempState = this.state.category;
		tempState.about={
			name:"About",
			currentMainContent:category	
		}
		this.setState({
			category:tempState
		})
		console.log("chooseSubContent");
		window.document.documentElement.scrollTop=0;		
	}
	chooseSubContent_mobile(event){
		let tempState = this.state.category;
		tempState.about={
			name:"About",
			currentMainContent:event.target.value	
		}
		this.setState({
			category:tempState
		})
		console.log("chooseSubContent_mobile");
		window.document.documentElement.scrollTop=0;					
	}
	chooseNavigation(category){
		// console.log("chooseNavigation");
		this.setState({
			currentNav:category
		});		
	}		

//---------------other_functions--------------------
	initiate(){
		let cookie_name=this.getCookie("sid");
		if(cookie_name){
			console.log("I got cookie sid="+cookie_name);
			let url =  'https://api-01.a45.me/api/web/Gateway.php';
	        let request = {
	            cmd: 'Login',
	            sid: cookie_name
			};			
			postRequest(url, request ,(response) => this.signInFunc(response,cookie_name));
		}else{
			console.log("I have no cookie");
		}
	}

	switchMobileMenu(){
		this.setState({
			mobileNav:!this.state.mobileNav
		})
	}

	logOut(){
		this.setState({
			currentMainContent:"Home",
			currentNav:"Classic",
			account:'',
			password:''	,
			sid:'',		
			category:{
				promotions:{
					name:"Promotions"
				},
				register:
				{
					name:"Register",
					detectSymbol:/[-~`!@#$%^&()_+{}?;:/><,*=\s]/,
					preventAlert:false,
					currency:'RMB',
					register_account:'',
					register_password:'',
					realName:'',
					repeatPS:'',
					agent:'',			
					email:'',
					phoneNumber:'',
					QQ:'',
					WC:''	
				},
				accountInfo:{
					name:"Account",
					account:'',
					username:'',
					balance:'',
					phone:'',
					currency:'',
					lastLogin:'',
					registerTime:''		
				},
				download:{
					name:"Download"
				},
				deposit:{
					name:"Deposit"
				},
				withdraw:{
					name:"Withdraw"
				},			
				gameHistory:{
					name:"GameHistory"
				},
				financialHistory:{
					name:"FinancialHistory"
				},
				about:{
					currentMainContent:"About"
				}				
			},
			mobileNav:false
		});
		this.setCookie("sid",null,0);
		this.chooseMainContent("Home")
	}	

//---------------render---------------
	renderSiderBar(){
		return(
		    <div className="topside">
		        <div id="toggleAD" className="side-menu">
		            <ul>
		                <li>
		                    <div >
		                        <i className="mps-help-man"></i>
		                        <h2>在线客户服务</h2>
		                        <p>24小时服务不间断</p>
		                    </div>
		                </li>
		                <li>
		                    <div >
		                        <i className="mps-help-qq"></i>
		                        <h2>QQ 客户服务</h2>
		                        <p>24小时服务不间断</p>
		                    </div>
		                </li>
		                <li>
		                    <div >
		                        <i className="mps-help-whatsapp"></i>
		                        <h2>Whatsapp</h2>
		                        <p>24小时服务不间断</p>
		                    </div>
		                </li>
		                <li>
		                    <div>
		                        <i className="mps-help-mail"></i>
		                        <h2>电子邮箱</h2>
		                        <p>lexisbetman@gmail.com</p>
		                    </div>
		                </li>
		                <li>
		                    <div >
		                        <i className="mps-quiz"></i>
		                        <h2>常见问题</h2>
		                        <p>解决各种疑难杂症</p>
		                    </div>
		                </li>
		                <li>
		                    <div >
		                        <i className="mps-service"></i>
		                        <h2>电话回拨</h2>
		                        <p>24小时服务不间断</p>
		                    </div>
		                </li>
		            </ul>
		        </div>
		        <div className="side-menu-mobile">
		            <div className="qr-list">
		                <h4>iOS 手机扫一扫</h4>
		                <img src="img/QRcode/QRcodeForIos.png" alt="presentation"/>
		                <h4>Android 扫一扫</h4>
		                <img src="img/QRcode/QRcodeForAndorid.png" alt="presentation"/>
		            </div>
		        </div>
		    </div>			
		);
	}
	renderSearching(){
		return(
		    <div className="offcanvas-search-wrapper">
		        <div className="offcanvas-search-inner">
		            <div className="offcanvas-close">
		                <i className="fa fa-close"></i>
		            </div>
		            <div className="container">
		                <div className="offcanvas-search-box">
		                    <form className="d-flex bdr-bottom w-100">
		                        <input type="text" placeholder="请再此处搜寻..."/>
		                        <button className="search-btn"><i className="fa fa-search"></i>search</button>
		                    </form>
		                </div>
		            </div>
		        </div>
		    </div>			
			);
	}
	renderFooter(){
		return(
			<div>
		        <div className="footer-bottom-area text-center mobileH">
		            <div className="container">
		                <div className="row">
		                    <div className="col-12">
		                        <div className="title">合作夥伴<span>Partner</span></div>
		                        <ul className="partnericons">
		                            <li className="G01">WM GAMING 完美集團</li>
		                            <li className="G02">LIXIN Casino</li>
		                            <li className="G02b">LIXIN 彩票</li>
		                            <li className="G03">eBET</li>
		                            <li className="G04">ALLBET 歐博</li>
		                            <li className="G05">Playtech</li>
		                            <li className="G06">JDB</li>
		                            <li className="G07">PNG PLAYnGO</li>
		                            <li className="G08">AG AsiaGaming</li>
		                            <li className="G09">Microgaming</li>
		                            <li className="G10">SBOBET</li>
		                            <li className="G11">MEGA WIN</li>
		                            <li className="G12">M8BET</li>
		                            <li className="G13">CMD368</li>
		                            <li className="G14">ORIENTAL GAME</li>
		                            <li className="G15">SUNBIT</li>
		                            <li className="G16">众发体育</li>
		                            <li className="G17">APOLLO</li>
		                            <li className="G18">DreamGaming</li>
		                            <li className="G19">KY開元</li>
		                            <li className="G20">SA GAMING</li>
		                            <li className="G21">Spadegaming</li>
		                            <li className="G22">金龍棋牌</li>
		                            <li className="G23">彩播</li>
		                            <li className="G24">實況鬥雞</li>
		                            <li className="G25">實況賽馬</li>
		                        </ul>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div className="footer-bottom-area copyT text-center">
		            <div className="container">
		                <div className="row">
		                    <div className="col-12">
		                        <p className="copyright">完美娱乐 Copyright &copy; <br></br>2020.WM Casino All rights reserved.</p>
		                    </div>
		                </div>
		            </div>
		        </div>				
			    <div className="modal" id="quick_view">
			        <div className="modal-dialog modal-lg modal-dialog-centered">
			            <div className="modal-content">
			                <div className="modal-header">
			                    <button type="button" className="close" data-dismiss="modal">&times;</button>
			                </div>
			                <div className="modal-body">
			                    <div className="product-details-inner">
			                        <div className="row">
			                            <div className="col-lg-5">
			                                <div className="product-large-slider">
			                                    <div className="pro-large-img">
			                                        <img src="img/product/product-details-img1.jpg" alt=""/>
			                                    </div>
			                                    <div className="pro-large-img">
			                                        <img src="img/product/product-details-img2.jpg" alt=""/>
			                                    </div>
			                                    <div className="pro-large-img">
			                                        <img src="img/product/product-details-img3.jpg" alt=""/>
			                                    </div>
			                                    <div className="pro-large-img">
			                                        <img src="img/product/product-details-img4.jpg" alt=""/>
			                                    </div>
			                                </div>
			                                <div className="pro-nav slick-row-10 slick-arrow-style">
			                                    <div className="pro-nav-thumb">
			                                        <img src="img/product/product-details-img1.jpg" alt=""/>
			                                    </div>
			                                    <div className="pro-nav-thumb">
			                                        <img src="img/product/product-details-img2.jpg" alt=""/>
			                                    </div>
			                                    <div className="pro-nav-thumb">
			                                        <img src="img/product/product-details-img3.jpg" alt=""/>
			                                    </div>
			                                    <div className="pro-nav-thumb">
			                                        <img src="img/product/product-details-img4.jpg" alt=""/>
			                                    </div>
			                                </div>
			                            </div>
			                            <div className="col-lg-7">
			                                <div className="product-details-des">
			                                    <h3 className="product-name">Premium Mens Sports Lather Keds</h3>
			                                    <div className="ratings d-flex">
			                                        <span><i className="fa fa-star"></i></span>
			                                        <span><i className="fa fa-star"></i></span>
			                                        <span><i className="fa fa-star"></i></span>
			                                        <span><i className="fa fa-star"></i></span>
			                                        <span><i className="fa fa-star"></i></span>
			                                        <div className="pro-review">
			                                            <span>1 Reviews</span>
			                                        </div>
			                                    </div>
			                                    <div className="price-box">
			                                        <span className="price-old"><del>$90.00</del></span>
			                                        <span className="price-regular">$70.00</span>
			                                    </div>
			                                    <h5 className="offer-text"><strong>Hurry up</strong>! offer ends in:</h5>
			                                    <div className="product-countdown" data-countdown="2019/09/20"></div>
			                                    <p className="pro-desc">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
			                                        eirmod tempor invidunt ut labore et dolore magna aliquyam erat.</p>
			                                    <div className="quantity-cart-box d-flex align-items-center">
			                                        <h6 className="option-title">qty:</h6>
			                                        <div className="quantity">
			                                            <div className="pro-qty"><input type="text"/></div>
			                                        </div>
			                                        <div className="action_link">
			                                            <div className="btn btn-cart2" >Add To Cart</div>
			                                        </div>
			                                    </div>
			                                    <div className="useful-links">
			                                        <div  data-toggle="tooltip" title="Compare"><i
			                                        className="fa fa-refresh"></i>compare</div>
			                                        <div  data-toggle="tooltip" title="Wishlist"><i
			                                        className="fa fa-heart-o"></i>wishlist</div>
			                                    </div>
			                                    <div className="like-icon">
			                                        <div className="facebook"><i className="fa fa-facebook"></i>like</div>
			                                        <div className="twitter"><i className="fa fa-twitter"></i>tweet</div>
			                                        <div className="pinterest"><i className="fa fa-pinterest"></i>save</div>
			                                        <div className="google"><i className="fa fa-google-plus"></i>share</div>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div> 
			                </div>
			            </div>
			        </div>
			    </div>				
			</div>
		);
	}






	render(){
		return(
			<div className="WM">
				{window.bMobile?
					<div className="turnPortrait"
    				>
    				</div>:""}	
				<Navigation
					account={this.state.account}
					password={this.state.password}
					sid={this.state.sid}
					category={this.state.category}
					handleChange={(event) => this.handleChange(event)}	
					submitLogin={(event) => this.submitLogin(event)}
					chooseContent={(category) => this.chooseMainContent(category)}
					prepareOpenGame={() => this.prepareOpenGame()}
					prepareSportGame={() => this.prepareSportGame()}
					logOut={() => this.logOut()}
					switchMobileMenu={() => this.switchMobileMenu()}
					mobileNav={this.state.mobileNav}
					test={() => console.log("test")}
				/>			
				{this.renderSiderBar()}
				{this.renderSearching()}
				<MainContent
					currentMainContent={this.state.currentMainContent}
					category={this.state.category}
					submitLogin={(event) => this.submitLogin(event)}	
					handleChange={(event) => this.handleChange(event)}
					submitRegister={(category) => this.submitRegister(category)}
					handleBlur={(event) => this.handleBlur(event)}
					chooseContent={(category) => this.chooseMainContent(category)}	
					prepareOpenGame={() => this.prepareOpenGame()}
					prepareSportGame={() => this.prepareSportGame()}					
					sid={this.state.sid}
					chooseSubContent={(category) => this.chooseSubContent(category)}
					currentSubContent={this.state.category.about.currentMainContent}
					chooseSubContent_mobile={(event) => this.chooseSubContent_mobile(event)}		
				/>				
				<div className="scroll-top not-visible">
					<i className="fa fa-angle-up"></i>
				</div>
				{this.renderFooter()}
			</div>
		);
	}
}

ReactDOM.render(
	<WM />,
	document.getElementById('root')
);

