import React from 'react';
import {InputItem} from './InputItem';
function Navigation(props) {
	return(
		<div className="Navigation" 
			style={{position: "fixed",width: "100%", zIndex:"5000" }}
    	>
	        <div className="main-header d-none d-lg-block">
	            <div className="header-top black-bg">
	                <div className="container">
	                    <div className="row align-items-center">
	                        <div className="col-lg-12 d-flex justify-content-end">
	                            {props.sid?
	                                <div className="navInfo">
	                                	<div className="navName">
	                                		{props.category.accountInfo.username}
	                                	</div>
                              			<div className="dollarAreaPC">
	                                		<i className="fa fa-dollar"></i>{props.category.accountInfo.balance}
	                                	</div>
	                                	<div className="navMyAcount"
	                                		 onClick={() => props.chooseContent(props.category.accountInfo.name)}
	                                		 >
		                                	<i className="fa fa-user-circle"></i> 
		                                	我的帐户
		                                </div>
                                		<div className="navLogout" 
                                			 onClick={() => props.logOut()}
                                			 >
                                			 登出
                                		</div>		                                
	                                </div>:
	                                <ul className="user-info-block">
										<li>
											<div className="col-lg-12 Htop">
			                                	<InputItem 
			                                		type={"text"}
			                                		placeholder={"請輸入用戶名"}
			                                		name={"account"}
			                                		value={props.account}
									                onChangeFun={(event) => props.handleChange(event)} 
									                required={true} 
			                                	/>				                                        
											</div>
										</li>
										<li className="Htoprealtive">
											<button type="submit" className="Htop btn btn-sqr2">忘记?</button>
											<form className="col-lg-12 Htop">
			                                	<InputItem 
			                                		type={"password"}
			                                		placeholder={"请输入密码"}
			                                		name={"password"}
			                                		value={props.password}
									                onChangeFun={(event) => props.handleChange(event)} 
									                required={true} 
			                                	/>				                                        
											</form>
										</li>
										<li><button onClick={(event) => props.submitLogin(event)} className="Htop btn btn-sqr">登入</button></li>
										<li><button onClick={() => props.chooseContent(props.category.register.name)} className="Htop btn btn-sqr">注册</button></li>
                            		</ul>
	                            }
                            	                            
	                            {/*<div className="top-left-navigation">
	                                <ul className="nav align-items-center">
	                                    <li className="language">
	                                        <img src="img/icon/cn.png" alt=""/> 简体中文 <i className="fa fa-angle-down"></i>
	                                        <ul className="dropdown-list">
	                                            <li><div ><img src="img/icon/cn.png" alt=""/> 简体中文</div></li>
	                                            <li><div ><img src="img/icon/tn.png" alt=""/> 繁體中文</div></li>
	                                            <li><div ><img src="img/icon/en.png" alt=""/> English</div></li>
	                                        </ul>
	                                    </li>
	                                </ul>
	                            </div>*/}
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div className="header-main-area black-soft sticky" >
	                <div className="container">
	                    <div className="row align-items-center position-relative">
	                        <div className="col-auto">
	                            <div className="logo" onClick={() => {props.chooseContent("Home");props.test()}}>
	                                <div>
	                                    <img src="img/logo/logo.png" alt=""/>
	                                </div>
	                            </div>
	                        </div>
	                        <div className="col-auto position-static">
	                            <div className="main-menu-area">
	                                <div className="main-menu">
	                                    <nav className="desktop-menu">
	                                        <ul>
	                                            <li onClick={() => props.chooseContent(props.category.promotions.name)}><div>优惠活动</div></li>
	                                            <li className="position-static"><div >电子竞技 <i className="fa fa-angle-down"></i></div>
	                                                <ul className="megamenu dropdown">
	                                                    <li className="mega-title">
	                                                        <div className="submenuIMG m21"></div>
	                                                    </li>
	                                                    <li className="mega-title">
	                                                        <div className="submenuIMG m22"></div>
	                                                    </li>
	                                                </ul>
	                                            </li>
	                                            <li className="position-static"><div >彩票游戏 <i className="fa fa-angle-down"></i></div>
	                                                <ul className="megamenu dropdown">
	                                                    <li className="mega-title"><div className="submenuIMG m31"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m32"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m33"></div></li>
	                                                </ul>
	                                            </li>
	                                            <li className="position-static">
	                                            	<div onClick={props.sid?() => props.prepareOpenGame():() => alert("請先登入")}>
	                                            		真人娱乐 
	                                            	</div>
	                                            </li>
	                                            <li className="position-static"><div >电子游艺 <i className="fa fa-angle-down"></i></div>
	                                                <ul className="megamenu dropdown">
	                                                    <li className="mega-title"><div className="submenuIMG m51"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m52"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m53"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m54"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m55"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m56"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m57"></div></li>
	                                                </ul>
	                                            </li>
	                                            <li className="position-static">
	                                            	<div onClick={props.sid?() => props.prepareSportGame():() => alert("請先登入")}>
	                                            		体育赛事
	                                            	</div>
	                                            </li>
	                                            <li className="position-static"><div >捕鱼游戏 <i className="fa fa-angle-down"></i></div>
	                                                <ul className="megamenu dropdown">
	                                                    <li className="mega-title"><div className="submenuIMG m71"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m72"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m73"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m74"></div></li>
	                                                </ul>
	                                            </li>
	                                            <li className="position-static"><div>动物竞技 <i className="fa fa-angle-down"></i></div>
	                                                <ul className="megamenu dropdown">
	                                                    <li className="mega-title"><div className="submenuIMG m81"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m82"></div></li>
	                                                </ul>
	                                            </li>
	                                            <li className="position-static"><div>棋牌 <i className="fa fa-angle-down"></i></div>
	                                                <ul className="megamenu dropdown">
	                                                    <li className="mega-title"><div className="submenuIMG m91"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m92"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m93"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m94"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m95"></div></li>
	                                                    <li className="mega-title"><div className="submenuIMG m96"></div></li>
	                                                </ul>
	                                            </li>
	                                            <li onClick={() => props.chooseContent(props.category.download.name)}>
	                                            	<div>手机下载</div>
	                                            </li>
	                                        </ul>
	                                    </nav>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div className="mobile-header d-lg-none d-md-block sticky black-soft">
	            <div className="container-fluid">
	                <div className="row align-items-center">
	                    <div className="col-12">
	                        <div className="mobile-main-header">
	                            <div className="mobile-logo" onClick={() => {props.chooseContent("Home")}}>
	                                <div>
	                                    <img src="img/logo/logo.png" alt=""/>
	                                </div>
	                            </div>
                                <div className="top-left-navigation">
                                    <ul className="nav align-items-center">
                                        <li className="language">
                                            {props.sid?props.category.accountInfo.username:<div onClick={() => {props.chooseContent("Mobile_Login")}}><i className="fa fa-sign-in"></i>登入</div>}
                                        </li>
                                    </ul>
                                </div>
                                {props.sid?
                                	<div onClick={() => props.logOut()}><span className="dollarArea"><i className="fa fa-dollar"></i>{props.category.accountInfo.balance}</span>登出</div>
                                	:	
                                	<div onClick={() => props.chooseContent(props.category.register.name)}>
                                		<i className="fa fa-id-card"></i> 
                                		注册
                                	</div>
                                }	                            
	                            <div className="mobile-menu-toggler" onClick={() => {props.switchMobileMenu()}}>
	                                <button className="mobile-menu-btn">
	                                    <span></span>
	                                    <span></span>
	                                    <span></span>
	                                </button>
	                            </div>
								{/*<div className="top-left-navigation">
	                                <ul className="nav align-items-center">
	                                    <li className="language">
	                                        <img src="img/icon/cn.png" alt=""/> 简 <i className="fa fa-angle-down"></i>
	                                        <ul className="dropdown-list">
	                                            <li><div ><img src="img/icon/cn.png" alt=""/> 简</div></li>
	                                            <li><div ><img src="img/icon/tn.png" alt=""/> 繁</div></li>
	                                            <li><div ><img src="img/icon/en.png" alt=""/> EN</div></li>
	                                        </ul>
	                                    </li>
	                                </ul>
								</div>*/}
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <aside className={props.mobileNav?"off-canvas-wrapper open":"off-canvas-wrapper"}>
	            <div className="off-canvas-overlay" onClick={() => {props.switchMobileMenu()}}></div>
	            <div className="off-canvas-inner-content">
	                <div className="btn-close-off-canvas">
	                    <i className="fa fa-close" onClick={() => {props.switchMobileMenu()}}></i>
	                </div>

	                <div className="off-canvas-inner">
	                    <div className="mobile-navigation">
	                        <nav>
	                            <ul className="mobile-menu">
	                                <li onClick={() => props.chooseContent(props.category.promotions.name)}>
	                                	<div>优惠活动</div>
	                                </li>
	                                <li>
	                                	<div onClick={props.sid?() => props.prepareOpenGame():() => alert("請先登入")}>
	                                		真人娱乐 
	                                	</div>
	                                </li>	
	                                <li className="menu-item-has-children ">
	                                	<div onClick={props.sid?() => props.prepareSportGame():() => alert("請先登入")}>
	                                		体育赛事
	                                	</div>
	                                </li>
	                                <li onClick={() => props.chooseContent(props.category.download.name)}>
	                                	<div>手机下载</div>
	                                </li>
	                            </ul>
	                        </nav>
	                    </div>
	                    <div className="mobile-settings">
	                        <ul className="nav">
	                            <li>
	                                <div className="dropdown mobile-top-dropdown">

	                                </div>
	                            </li>
	                            <li>
	                                <div className="dropdown mobile-top-dropdown">
	                                {props.sid?
	                                	<div className="dropdown-toggle"
	                                    	 onClick={() => props.chooseContent(props.category.accountInfo.name)}
	                                    >
	                                        我的帐户
	                                    </div>
	                                    :""
	                                }

	                                    <div className="dropdown-menu" aria-labelledby="myaccount">
	                                        <div className="dropdown-item"  target="_blank">我的帐户</div>
	                                        <div className="dropdown-item"  target="_blank">充值</div>
	                                        <div className="dropdown-item"  target="_blank">提款</div>
	                                        <div className="dropdown-item"  target="_blank">游戏记录</div>
	                                        <div className="dropdown-item"  target="_blank">财务记录</div>
	                                    </div>
	                                </div>
	                            </li>
	                        </ul>
	                    </div>
	                </div>
	            </div>
	        </aside>
			<div className="FoooterBar">
				<div onClick={() => {props.chooseContent("Home")}}><i className="fa fa-home"></i>首页</div>
				<div onClick={() => {props.chooseContent("Promotions")}}><i className="fa fa-gift"></i>优惠</div>
				<div onClick={() => {props.chooseContent("Download")}}><i className="fa fa-gamepad"></i>下载</div>
				<div onClick={props.sid?() => props.chooseContent("Deposit"):() => alert("請先登入")}><i className="fa fa-credit-card"></i>充值</div>
				<div onClick={props.sid?() => props.chooseContent("Withdraw"):() => alert("請先登入")}><i className="fa fa-money"></i>提现</div>
				<div onClick={props.sid?() => props.chooseContent("Account"):() => alert("請先登入")}><i className="fa fa-id-card"></i>帐户</div>
			</div>	        
	    </div> 
	);
}


export {Navigation};