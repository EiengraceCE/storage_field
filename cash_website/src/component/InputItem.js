import React from 'react';

function rotateBoolean(){
    window.bRotate=!window.bRotate;
}

function InputItem(props){
    return(
        <div className="single-input-item">
            <input 
                onFocus={rotateBoolean}
                type={props.type} 
                placeholder={props.placeholder} 
                name={props.name} 
                value={props.value} 
                onChange={props.onChangeFun} 
                required={props.required?props.required:false} 
                onBlur={props.onBlurFun?props.onBlurFun:rotateBoolean}
                autoComplete="off"
            />
            {props.tip?<span className="psTinfo">{props.tip}</span>:false}
        </div>          
    );
}
export {InputItem};