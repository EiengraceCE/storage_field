import React from 'react';
import { useState } from 'react';

function PromotionItem(props){
    const [fold, setFold] = useState(false);
    return(
        <section className="sectionFAQs"
                 id={props.ordinal}
                 onClick={() => setFold(!fold)}
        >
            <span className="myanchor" id={props.anchor}></span>
            <div className="TbarArea">
                <div className="TbarT">{props.title}</div>
                <div className="TbarSub">{props.summary}</div>
                <div className="TbarDate">{props.duration}</div>
            </div>
            <div className="faqsBIMG"></div>
            <div className={fold?"faqsUnfold":"faqsBIFO"}>
                <div className="infoCT">
                    {props.infoDetail}
                    {props.notification}
                </div>
            </div>
        </section>          
    );
}

export {PromotionItem};