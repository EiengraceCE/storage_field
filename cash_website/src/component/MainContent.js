import React from 'react';
import {InputItem} from './InputItem';
import {PromotionItem} from './PromotionItem';
import {SubContent} from './subContent';
import Scrollspy from 'react-scrollspy'
function MainContent(props){
		// console.log(props);
	switch(props.currentMainContent){
		case "Home":
			return (
				<div className="maincontent">
					{/*↓top AD area - Start*/}
					<section className="hero-slider">
						<div className="hero-slider-active slick-arrow-style slick-arrow-style_hero slick-dot-style">
							<div className="hero-single-slide hero-overlay">
								<div className="hero-slider-item hero-1 bg-img">
									<div className="container">
										<div className="row">
											<div className="col-md-12">
												<div className="hero-slider-content slide-1">
													<h1 className="slide-title">New WM</h1>
													<h2 className="slide-subtitle">WM 真人娱乐 <span>就是要你乐</span></h2>
													<div className="btn btn-large btn-bg"
														 onClick={props.sid?() => props.prepareOpenGame():() => alert("請先登入")}
													>Play Now
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					{/*↑top AD area - End*/}
			
			        <section className="product-gallery section-padding">
			            <div className="container">
			                <div className="row mobileAREA">
								<div className="H_allGames">
									<div onClick={props.sid?() => props.prepareOpenGame():() => alert("請先登入")}>真人娱乐</div>
									<div onClick={props.sid?() => props.prepareSportGame():() => alert("請先登入")}>体育赛事</div>
								</div>
			                </div>
							<div className="row mobileP1">
			                    <div className="col-12">
			                        <div className="section-title text-center">
			                            <h3 className="title">真实的视觉和听觉体验</h3>
			                        </div>
			                    </div>
			                </div>
			                <div className="row">
			                    <div className="col-12 mobileP2">
			                        <div className="product-container">
			                            <div className="product-tab-menu">
			                                <ul className="nav justify-content-center">
			                                    <li><div className="active" data-toggle="tab">WM 真人娱乐</div></li>
			                                    <li><div data-toggle="tab">SBO 体育博彩</div></li>
			                                </ul>
			                            </div>
			                            <div className="tab-content">
			                                <div className="tab-pane fade show active" id="tab1">
			                                    <div className="row">
													<div className="col-lg-2 col-sm-12"></div>
													<div className="col-lg-4 col-sm-6">
														<div className="product-item">
															<div className="product-thumb">
																<div><img src="img/homegames/home-cate-game-01.jpg" alt=""/></div>
																<div className="button-group">
																	<div data-toggle="tooltip" title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
																</div>
																<div className="product-label">
																	<span>new</span>
																</div>
															</div>
															<div className="product-content">
																<div className="product-caption">
																	<h5 className="product-name">
																		<div>WM 真人娱乐</div>
																	</h5>
																	<div className="price-box">
																		<span className="price-regular">最高标准私人享受</span>
																	</div>
																</div>
															</div>
														</div>	                                        
			                                    	</div>
													<div className="col-lg-4 col-sm-6">
														<div className="product-item">
															<div className="product-thumb">
																<div><img src="img/homegames/home-cate-game-02.jpg" alt=""/></div>
																<div className="button-group">
																	<div data-toggle="tooltip" title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
																</div>
																<div className="product-label">
																	<span>new</span>
																</div>
															</div>
															<div className="product-content">
																<div className="product-caption">
																	<h6 className="product-name">
																		<div>SBO 体育博彩</div>
																	</h6>
																	<div className="price-box">
																		<span className="price-regular">流畅视讯体验</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div className="col-lg-2 col-sm-12"></div>	
												</div>
											</div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			                <div className="row mtn-30 PoRel">
								<div className="i_RB"></div>
			                    <div className="col-lg-4 col-sm-6">
			                        <div className="footer-widget-item mt-30 service-time-box home-box">
			                            <h6 className="widget-title">强势服务</h6>
			                            <ul className="usefull-links area1">
			                                <li>
			                                    <div className="time-item">
			                                        <div className="time-text">
			                                            <div className="fl">
			                                                <h5>存款火速到账</h5>平均时间
			                                            </div>
			                                            <div className="fr">
			                                                <strong id="sceValu">23</strong> 秒
			                                            </div>
			                                        </div>
			                                        <div className="bar">
			                                            <i style={{width: "45%"}}></i>
			                                        </div>
			                                    </div>
			                                </li>
			                                <li>
			                                    <div className="time-item">
			                                        <div className="time-text">
			                                            <div className="fl">
			                                                <h5>取款火速到账</h5>平均时间
			                                            </div>
			                                            <div className="fr thpoint">
			                                                <strong id="sceValu2">200</strong> 秒
			                                            </div>
			                                        </div>
			                                        <div className="bar">
			                                            <i style={{width: "72%"}}></i>
			                                        </div>
			                                    </div>
			                                </li>
			                            </ul>
			                        </div>
			                    </div>
			                    <div className="col-lg-4 col-sm-6">
			                        <div className="footer-widget-item mt-30">
			                            <h6 className="widget-title">关于我们</h6>
			                            <ul className="usefull-links area3">
			                                <li onClick={() => {props.chooseContent(props.category.about.name);props.chooseSubContent("About")}}><div>关于WM</div></li>
											<li onClick={() => {props.chooseContent(props.category.about.name);props.chooseSubContent("Agreement")}}><div>用户协议</div></li>
											<li onClick={() => {props.chooseContent(props.category.about.name);props.chooseSubContent("ReasonableGaming")}}><div>理性博彩</div></li>
											<li onClick={() => {props.chooseContent(props.category.about.name);props.chooseSubContent("Disclaimer")}}><div>免责声明</div></li>
											<li onClick={() => {props.chooseContent(props.category.about.name);props.chooseSubContent("PersonalPrivacy")}}><div>用户隐私</div></li>
			                            </ul>
			                        </div>
			                    </div>
			                    <div className="col-lg-4 col-sm-6">
			                        <div className="footer-widget-item mt-30">
			                            <h6 className="widget-title">帮助中心</h6>
			                            <ul className="usefull-links area3">
			                                <li onClick={() => {props.chooseContent(props.category.about.name);props.chooseSubContent("FAQ")}}><div>常见问题</div></li>
			                                <li onClick={() => {props.chooseContent(props.category.about.name);props.chooseSubContent("DepositSupport")}}><div>存款帮助</div></li>
			                                <li onClick={() => {props.chooseContent(props.category.about.name);props.chooseSubContent("WithdrawSupport")}}><div>取款帮助</div></li>
			                                <li onClick={() => {props.chooseContent(props.category.about.name);props.chooseSubContent("ContactUs")}}><div>联系我们</div></li>
			                            </ul>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </section>				
				</div>
			);
		case "Register":
			// console.log("registerContent");
			return (
				<div className="maincontent">
			        <div className="breadcrumb-area breadcrumb-img bg-img accBG">
			            <div className="container">
			                <div className="row">
			                    <div className="col-12">
			                        <div className="breadcrumb-wrap">
			                            <nav aria-label="breadcrumb">
			                                <h3 className="breadcrumb-title">REGISTER</h3>
			                                <ul className="breadcrumb justify-content-center">
			                                    <li className="breadcrumb-item"><div><i className="fa fa-home"></i></div></li>
			                                    <li className="breadcrumb-item active" aria-current="page">免费注册</li>
			                                </ul>
			                            </nav>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			        <div className="login-register-wrapper section-padding">
			            <div className="container">
			                <div className="member-area-from-wrap">
			                    <div className="row">
			                        <div className="col-lg-12">
			                            <div className="acc login-reg-form-wrap sign-up-form newM">
			                                <h4>免费注册 / Sign Up</h4>
			                                <span className="psTred">有"*"&nbsp;号为必填项目</span>
			                                <form onSubmit={props.submitRegister}>
			                                	<InputItem 
			                                		type={"text"}
			                                		placeholder={"请输入真实姓名"}
			                                		name={"realName"}
			                                		value={props.category.register.realName}
									                onChangeFun={(event) => props.handleChange(event)} 
									                required={false} 
									                tip={"必须与您的银行帐户一致，否则无法进行取款"}
			                                	/>
			                                	<InputItem 
			                                		type={"text"}
			                                		placeholder={"请输入账号"}
			                                		name={"register_account"}
			                                		value={props.category.register.register_account}
									                onChangeFun={(event) => props.handleChange(event)} 
									                required={false} 
									                tip={"需为5~17个英文或数字的组合"}
			                                	/>				                                		                              			                                	
			                                    <div className="row">
			                                        <div className="col-lg-6">
					                                	<InputItem 
					                                		type={"password"}
					                                		placeholder={"请输入密码"}
					                                		name={"register_password"}
					                                		value={props.category.register.register_password}
											                onChangeFun={(event) => props.handleChange(event)} 
											                required={true} 
											                tip={"需为6~20个英文与数字的组合"}
					                                	/>				                                        
			                                        </div>
			                                        <div className="col-lg-6">
					                                	<InputItem 
					                                		type={"password"}
					                                		placeholder={"请再次输入密码"}
					                                		name={"repeatPS"}
					                                		value={props.category.register.repeatPS}
											                onChangeFun={(event) => props.handleChange(event)} 
											                required={true} 
											                onBlurFun={(event) => props.handleBlur(event)}
					                                	/>				                                        
			                                        </div>
			                                    </div>
												{/*<div className="select">
													<select name='currency' value={props.category.register.currency} onChange={props.handleChange}>
														<option value="RMB">RMB</option>
														<option value="USD">USD</option>
														<option value="NTD">NTD</option>
														<option value="THB">THB</option>
														<option value="MYR">MYR</option>
													</select>
												</div>*/}
			                                	<InputItem 
			                                		type={"text"}
			                                		placeholder={"请输入代理账号"}
			                                		name={"agent"}
			                                		value={props.category.register.agent}
									                onChangeFun={(event) => props.handleChange(event)} 
									                tip={"若有代理，请输入其ID"}
			                                	/>	
			                                	<InputItem 
			                                		type={"email"}
			                                		placeholder={"请输入信箱"}
			                                		name={"email"}
			                                		value={props.category.register.email}
									                onChangeFun={(event) => props.handleChange(event)} 
			                                	/>		
			                                	<InputItem 
			                                		type={"text"}
			                                		placeholder={"请输入手机号码"}
			                                		name={"phoneNumber"}
			                                		value={props.category.register.phoneNumber}
									                onChangeFun={(event) => props.handleChange(event)} 
			                                	/>					                                				                                													
			                                    <div className="row">
			                                        <div className="col-lg-6">
					                                	<InputItem 
					                                		type={"password"}
					                                		placeholder={"请输入QQ"}
					                                		name={"QQ"}
					                                		value={props.category.register.QQ}
											                onChangeFun={(event) => props.handleChange(event)} 
					                                	/>				                                        
			                                        </div>
			                                        <div className="col-lg-6">
					                                	<InputItem 
					                                		type={"password"}
					                                		placeholder={"请输入微信"}
					                                		name={"WC"}
					                                		value={props.category.register.WC}
											                onChangeFun={(event) => props.handleChange(event)} 
					                                	/>				                                        
			                                        </div>
			                                    </div>
			                                    <div className="acc single-input-item">
			                                        <div className="login-reg-form-meta">
			                                            <div className="remember-meta">		                                				                                            	
			                                                <div className="custom-control custom-checkbox">
							                                	<InputItem 
							                                		type={"checkbox"}
							                                		required={false}
							                                		tip={"我已屆滿合法博彩年齡，且同意各項\"用戶協議\""}
							                                	/>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div className="single-input-item">
			                                        <button type="submit" className="btn btn-sqr">免费注册</button>
			                                    </div>
			                                </form>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>				
				</div>
			);
		case "Mobile_Login":
			return (
				<div className="maincontent">
			        <div className="breadcrumb-area breadcrumb-img bg-img accBG">
			            <div className="container">
			                <div className="row">
			                    <div className="col-12">
			                        <div className="breadcrumb-wrap">
			                            <nav aria-label="breadcrumb">
			                                <h3 className="breadcrumb-title">LOGIN</h3>
			                                <ul className="breadcrumb justify-content-center">
			                                    <li className="breadcrumb-item"><div><i className="fa fa-home"></i></div></li>
			                                    <li className="breadcrumb-item active" aria-current="page">登录</li>
			                                </ul>
			                            </nav>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			        <div className="login-register-wrapper section-padding MBlogin">
			            <div className="container">
			                <div className="member-area-from-wrap">
			                    <div className="row">
			                        <div className="col-lg-12">
			                            <div className="acc login-reg-form-wrap sign-up-form MacBG">
											<div className="RTstyle"></div>
			                                <h4>登录 / Sign In</h4>
                                            <form onSubmit={props.submitLogin}>
			                                	<InputItem 
			                                		type={"text"}
			                                		placeholder={"请输入信箱或会员账号"}
			                                		name={"account"}
			                                		value={props.account}
									                onChangeFun={(event) => props.handleChange(event)} 
									                required={false}
			                                	/>
			                                	<InputItem 
			                                		type={"password"}s
			                                		placeholder={"请输入密码"}
			                                		name={"password"}
			                                		value={props.password}
									                onChangeFun={(event) => props.handleChange(event)} 
									                required={false} 
			                                	/>										                                			 
                                                {/*<div className="single-input-item">
                                                    <div className="login-reg-form-meta d-flex align-items-center justify-content-between">
                                                        <div className="remember-meta">
                                                            <div className="custom-control custom-checkbox">
							                                	<InputItem 
							                                		type={"checkbox"}
													                tip={"記住我"} 
							                                	/>					                                                                    
                                                            </div>
                                                        </div>
                                                        <span className="forget-pwd">忘记密码?</span>
                                                    </div>
												</div>*/}
                                                <div className="single-input-item">
                                                    <button type="submit" className="btn btn-sqr">登录</button>
                                                </div>
                                            </form>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>				
				</div>
			);		
		case "Account":
			// console.log("Account");
			return (
				<div className="maincontent">
		            <div className="header-main-area black-soft sticky mCCBar">
		                <div className="container">
		                    <div className="row align-items-center position-relative">
		                        <div className="col-auto position-static">
		                            <div className="main-menu-area">
		                                <div className="main-menu">
		                                    <nav className="desktop-menu">
		                                        <ul>
		                                            <li onClick={() => props.chooseContent(props.category.accountInfo.name)}><div><i className="fa fa-id-card"></i>我的帐户</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.deposit.name)}><div><i className="fa fa-credit-card"></i>充值</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.withdraw.name)}><div><i className="fa fa-money"></i>提款</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.gameHistory.name)}><div><i className="fa fa-history"></i>游戏记录</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.financialHistory.name)}><div><i className="fa fa-clipboard"></i>财务记录</div></li>
		                                        </ul>
		                                    </nav>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>						
			        <div className="checkout-page-wrapper section-padding accStyle">
			            <div className="container">
			                <div className="row">
			                    <div className="col-lg-7">
			                        <div className="checkout-billing-details-wrap">
			                            <h5 className="checkout-title">
			                                个人资料<button title="" className="nrc-button">修改 登录 / 取款 密码</button>
			                            </h5>
			                            <div className="billing-form-wrap">
			                                <div className="resume-details">
			                                    <div className="resume-item">
			                                        <p className="title">注册时间：</p>
			                                        <p className="value">{props.category.accountInfo.registerTime}</p>
			                                    </div>
			                                    <div className="resume-item">
			                                        <p className="title">上次登录时间：</p>
			                                        <p className="value">{props.category.accountInfo.lastLogin}</p>
			                                    </div>
			                                    <div className="resume-item">
			                                        <p className="title">会员账号：</p>
			                                        <p className="value">{props.category.accountInfo.account}</p>
			                                    </div>
			                                    <div className="resume-item">
			                                        <p className="title">真实姓名：</p>
			                                        <p className="value">{props.category.accountInfo.username}</p>
			                                    </div>
			                                    <div className="resume-item">
			                                        <p className="title">手机号码：</p>
			                                        <p className="value">{props.category.accountInfo.phone}</p>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                    <div className="col-lg-5">
			                        <div className="order-summary-details">
			                            <div className="order-summary-content">
			                                <div>
			                                    <div className="box bank-card">
			                                        <div className="title">银行卡资料</div>
			                                        <div className="add-info">
			                                            <i className="fa fa-plus-circle"></i>
			                                            <div>
			                                                <div className="add-info-title">新增银行卡</div>
			                                                <div className="add-info-dec">请先新增银行卡资料</div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                <div className="wallets box">
			                                    <div className="title">
			                                        <div>所有钱包</div>
			                                        <div className="currency"></div>
			                                    </div>
			                                    <div className="price">{props.category.accountInfo.balance}</div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
		        </div>
			);				
		case "Promotions":
            return (
            	<div className="maincontent">
	                <div className="breadcrumb-area breadcrumb-img bg-img eventBG">
			            <div className="container">
			                <div className="row">
			                    <div className="col-12">
			                        <div className="breadcrumb-wrap">
			                            <nav aria-label="breadcrumb">
			                                <h3 className="breadcrumb-title">HOT NEWS</h3>
			                                <ul className="breadcrumb justify-content-center">
			                                    <li className="breadcrumb-item"><div><i className="fa fa-home"></i></div></li>
			                                    <li className="breadcrumb-item active" aria-current="page">优惠活动</li>
			                                </ul>
			                            </nav>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
					<div className="insideCT" style={{display:"flex"}}>
	                	<div className="promotionTab">
							<div className="col-lg-3 col-sm-0 newsRM">
							    <Scrollspy 
							    	items={ ['Promotion_first', 'Promotion_second', 'Promotion_third', 'Promotion_fourth', 'Promotion_fifth', 'Promotion_sixth'] } 
							    	currentClassName="is_current" 
							    	className="list-group left-category selectList"
							    	offset={ -100 }
							    >
							        <a  href="#anchor_first" 
							            className="list-group-item list-group-item-action">
							            <div className="my-0">全部</div>
							        </a>
							        <a  href="#anchor_second"
							            className="list-group-item list-group-item-action">
							            <div className="my-0">充值獎勵</div>
							        </a>
							        <a  href="#anchor_third"
							            className="list-group-item list-group-item-action">
							            <div className="my-0">簽到獎勵</div>
							        </a>
							        <a  href="#anchor_fourth"
							            className="list-group-item list-group-item-action">
							            <div className="my-0">成就獎勵</div>
							        </a>
							        <a  href="#anchor_fifth"
							            className="list-group-item list-group-item-action">
							            <div className="my-0">返水活動</div>
							        </a>

							        {/*<a  href="#Promotion_sixth"
							            className="list-group-item list-group-item-action">
							            <div className="my-0">看板活動</div>
							        </a>*/}
							    </Scrollspy>
							</div>
						</div>						
						<div className="col-lg-9 newsLA">   
		                    <PromotionItem
		                    	ordinal={"Promotion_first"}
		                    	anchor={"anchor_first"}
		                    	title={"天天玩，天天送回馈"}
		                    	summary={"天天返水红利高达30%无上限!"}
		                    	duration={"活动时间:2020年05月20日"}
		                    	infoDetail={<div className="infoDetail">
		                                        即日起，注册的会员，每日电子游戏有效投注达到100元或以上即可获得相应1%回馈彩金，<br></br>
		                                        赶快邀请小伙伴注册账号拿礼金吧！<br></br>
		                                        活动对象每日有效投注可获彩金流水
											</div>}
								notification={<div>
			                                    <div className="TTbar">领取方式</div>
			                                    次日18点前，系统自动派送<br></br>
			                                    例如：会员A当日有效投注达到5000，次日即可获得50元投注回馈。
			                                    <div className="TTbar">申请方式</div>
			                                    会员无需申请，系统将于次日18点前自动派送到会员账号。
			                                    <div className="TTbar">活动细则</div>
			                                    <ul>
			                                        <li>所获彩金只需1倍下注即可提现。</li>
			                                        <li>日结区间以北京时间当日00:00开始至次日北京时间23:59计算。</li>
			                                        <li>所有优惠特为玩家而设，如发现任何团体或个人，以不诚实方式套取红利或任何威胁、滥用公司优惠等行为，公司保留冻结、取消该团体或个人账户及账户结余的权利。</li>
			                                        <li>若会员对活动有争议时，为确保双方利益，杜绝身份盗用行为，本站有权要求会员向我们提供充足有效的文件，用以确认是否享有该优惠的资质。</li>
			                                        <li>此活动可与其他优惠共享。</li>
			                                        <li>本站保留所有权利在任何时候都可以更改、停止、取消该优惠活动。</li>
			                                    </ul>											
											  </div>}			
		                    />  
		                    <PromotionItem
		                    	ordinal={"Promotion_second"}
		                    	anchor={"anchor_second"}
		                    	title={"存款抢红包"}
		                    	summary={"红包雨下不停 高额红包会员独享"}
		                    	duration={"活动时间:2020年05月20日"}
		                    	infoDetail={<div className="infoDetail">
		                                        即日起，抢红包活动全面升级！<br></br>
		                                        凡是存款100元以上的会员皆可参加现金抢红包活动，<br></br>
		                                        点选活动中 "进行抢红包"参与抢红包，单个红包最高8888元，快来试试您的运气吧！<br></br>
		                                        更多给力现金回馈活动筹备中，敬请关注。<br></br>
											</div>}
								notification={<div>
			                                    <div className="TTbar">活动细则</div>
			                                    <ul>
			                                        <li>会员在当日存款达到100+，即可马上获得抢红包机会。</li>
			                                        <li>所获得奖金需流水=(存款+红包金额)*1，即可提款。</li>
			                                        <li>会员存款数据均由系统自动统计，若有任何异议，以本站核定为准不得争议。</li>
			                                        <li>当日的存款记录计算方式采用的是北京时间，即北京时间每天00:00至23:59。</li>
			                                        <li>如果您当日达到抢红包条件，请于当日23:59前进行参与，超过限定抢红包时间将视为会员自动弃权，不得争议！</li>
			                                        <li>所有数据皆以系统数据为准。</li>
			                                        <li>如发现会员同一个IP下注册多个账号进行投注抢红包，公司有权拒绝赠送其彩金并做账号冻结处理，保证正常玩家的利益。</li>
			                                        <li>所抢红包金额系统自动派发，无需申请,秒抢,秒到账。</li>
			                                        <li>此抢红包活动为系统程序自动运行，红包的概率完全遵循力学及自然概率，不涉及任何人工操作，抽奖结果以系统判定为准，不得争议。</li>
			                                        <li>如您忘记会员账号/密码，请您联系24小时在线客服协助您取回您的账号信息。</li>
			                                        <li>参与该优惠，即表示您同意《优惠规则与条款》</li>
			                                    </ul>
			                                    <span>温馨提示</span>
			                                    亲们！尚未注册/存款的亲们强烈建议您注册/存款，每天在进行存款游戏，天天参与多项优惠活动噢！如此给力，速来尽情玩乐吧！											
											  </div>}			
		                    />  
		                    <PromotionItem
		                    	ordinal={"Promotion_third"}
		                    	anchor={"anchor_third"}
		                    	title={"新会员领礼金"}
		                    	summary={"新会员注册 迎星礼拿不停"}
		                    	duration={"活动时间:2020年05月20日"}
		                    	infoDetail={<div className="infoDetail">
		                                        于北京时间2019年11月1日起，会员注册后首次充值达到100+，<br></br>
		                                        即可申请首存大方送优惠，存多少送多少！！！<br></br>
		                                        首次入款存款优惠流水要求公式 : (存款+奖金)*20倍：此优惠不限游戏。<br></br>
		                                        注：此活动仅限会员第一笔入款参与。例如：会员首次入款100元，即可获得100元优惠彩金，达到4000的打码量即可提交出款。（100+100）X20倍=4000元
											</div>}
								notification={<div>
			                                    <div className="TTbar">申请方式</div>
			                                    会员首次入款成功后未下注前，点击 "申请优惠" 进行申请；<br></br>
			                                    审核通过后活动部门会将相应彩金添加到您的游戏账号上。
			                                    <div className="TTbar">活动细则</div>
			                                    <ul>
			                                        <li>本金和优惠彩金仅需达到相应的流水倍数即可提现。</li>
			                                        <li>所有优惠特为玩家而设，如发现任何团体或个人，以不诚实方式套取红利或任何威胁、滥用公司优惠等行为，公司保留冻结、取消该团体或个人账户及账户结余的权利。</li>
			                                        <li>会员有重复申请账号行为时，本站保留取消、收回会员优惠彩金的权利，甚至没收会员账号余额的权利。每一位玩家、每一地址、每一电子邮箱、每一电话号码、相同支付卡/信用卡号码、同一IP、以及共享电脑环境（例如网吧、其它公用电脑等）仅限申请一次相关存款优惠。</li>
			                                        <li>若会员对活动有争议时，为确保双方利益，杜绝身份盗用行为，本站有权要求会员向我们提供充足有效的文件，用以确认是否享有该优惠的资质。</li>
			                                        <li>此活动可与其它优惠同时共享。</li>
			                                        <li>本站保留所有权利在任何时候都可以更改、停止、取消该优惠活动。</li>
			                                    </ul>											
											  </div>}			
		                    />
		                    <PromotionItem
		                    	ordinal={"Promotion_fourth"}
		                    	anchor={"anchor_fourth"}
		                    	title={"新会员领礼金"}
		                    	summary={"新会员注册 迎星礼拿不停"}
		                    	duration={"活动时间:2020年05月20日"}
		                    	infoDetail={<div className="infoDetail">
		                                        于北京时间2019年11月1日起，会员注册后首次充值达到100+，<br></br>
		                                        即可申请首存大方送优惠，存多少送多少！！！<br></br>
		                                        首次入款存款优惠流水要求公式 : (存款+奖金)*20倍：此优惠不限游戏。<br></br>
		                                        注：此活动仅限会员第一笔入款参与。例如：会员首次入款100元，即可获得100元优惠彩金，达到4000的打码量即可提交出款。（100+100）X20倍=4000元
											</div>}
								notification={<div>
			                                    <div className="TTbar">申请方式</div>
			                                    会员首次入款成功后未下注前，点击 "申请优惠" 进行申请；<br></br>
			                                    审核通过后活动部门会将相应彩金添加到您的游戏账号上。
			                                    <div className="TTbar">活动细则</div>
			                                    <ul>
			                                        <li>本金和优惠彩金仅需达到相应的流水倍数即可提现。</li>
			                                        <li>所有优惠特为玩家而设，如发现任何团体或个人，以不诚实方式套取红利或任何威胁、滥用公司优惠等行为，公司保留冻结、取消该团体或个人账户及账户结余的权利。</li>
			                                        <li>会员有重复申请账号行为时，本站保留取消、收回会员优惠彩金的权利，甚至没收会员账号余额的权利。每一位玩家、每一地址、每一电子邮箱、每一电话号码、相同支付卡/信用卡号码、同一IP、以及共享电脑环境（例如网吧、其它公用电脑等）仅限申请一次相关存款优惠。</li>
			                                        <li>若会员对活动有争议时，为确保双方利益，杜绝身份盗用行为，本站有权要求会员向我们提供充足有效的文件，用以确认是否享有该优惠的资质。</li>
			                                        <li>此活动可与其它优惠同时共享。</li>
			                                        <li>本站保留所有权利在任何时候都可以更改、停止、取消该优惠活动。</li>
			                                    </ul>											
											  </div>}			
		                    /> 
		                    <PromotionItem
		                    	ordinal={"Promotion_fifth"}
		                    	anchor={"anchor_fifth"}
		                    	title={"新会员领礼金"}
		                    	summary={"新会员注册 迎星礼拿不停"}
		                    	duration={"活动时间:2020年05月20日"}
		                    	infoDetail={<div className="infoDetail">
		                                        于北京时间2019年11月1日起，会员注册后首次充值达到100+，<br></br>
		                                        即可申请首存大方送优惠，存多少送多少！！！<br></br>
		                                        首次入款存款优惠流水要求公式 : (存款+奖金)*20倍：此优惠不限游戏。<br></br>
		                                        注：此活动仅限会员第一笔入款参与。例如：会员首次入款100元，即可获得100元优惠彩金，达到4000的打码量即可提交出款。（100+100）X20倍=4000元
											</div>}
								notification={<div>
			                                    <div className="TTbar">申请方式</div>
			                                    会员首次入款成功后未下注前，点击 "申请优惠" 进行申请；<br></br>
			                                    审核通过后活动部门会将相应彩金添加到您的游戏账号上。
			                                    <div className="TTbar">活动细则</div>
			                                    <ul>
			                                        <li>本金和优惠彩金仅需达到相应的流水倍数即可提现。</li>
			                                        <li>所有优惠特为玩家而设，如发现任何团体或个人，以不诚实方式套取红利或任何威胁、滥用公司优惠等行为，公司保留冻结、取消该团体或个人账户及账户结余的权利。</li>
			                                        <li>会员有重复申请账号行为时，本站保留取消、收回会员优惠彩金的权利，甚至没收会员账号余额的权利。每一位玩家、每一地址、每一电子邮箱、每一电话号码、相同支付卡/信用卡号码、同一IP、以及共享电脑环境（例如网吧、其它公用电脑等）仅限申请一次相关存款优惠。</li>
			                                        <li>若会员对活动有争议时，为确保双方利益，杜绝身份盗用行为，本站有权要求会员向我们提供充足有效的文件，用以确认是否享有该优惠的资质。</li>
			                                        <li>此活动可与其它优惠同时共享。</li>
			                                        <li>本站保留所有权利在任何时候都可以更改、停止、取消该优惠活动。</li>
			                                    </ul>											
											  </div>}			
		                    /> 	                     	                    	                                                      
	                	</div>
					</div>
				</div>          
            );
		case "Download":
			return (
				<div className="maincontent">
			        <div className="breadcrumb-area breadcrumb-img bg-img phonedownloadBG">
			            <div className="container">
			                <div className="row">
			                    <div className="col-12">
			                        <div className="breadcrumb-wrap">
			                            <nav aria-label="breadcrumb">
			                                <h3 className="breadcrumb-title">MOBILE DOWNLOAD</h3>
			                                <ul className="breadcrumb justify-content-center">
			                                    <li className="breadcrumb-item"><div><i className="fa fa-home"></i></div></li>
			                                    <li className="breadcrumb-item active" aria-current="page">手机下载</li>
			                                </ul>
			                            </nav>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
						<div className="app-body">
			            <div className="qr">
			                <div className="main-block">
			                    <div className="main-wrap">
			                        <div className="mobile-block">
			                            <div className="m-screen"><img src="img/mobiledownload/screen.jpg" alt=""/></div>
			                        </div>
			                        <div className="qr-block">
			                            <h2>机会一手把握！</h2>
			                            <div className="des">全面支持苹果APP 安卓APP 手机全部浏览器移动 技术领走在行业尖端
			                            </div>
			                            <div className="qrcode">手机扫描二维码即可下载
			                                <div className="qr-item">
			                                    <div className="qr-android">
			                                        <span className="phsys fa fa-android"></span>
			                                        <img src="img/mobiledownload/android_QRcode.png" alt=""/>
			                                        <h4>Android 扫一扫 <i className="mps-quiz"></i></h4>
			                                    </div>
			                                    <div className="qr-ios">
			                                        <span className="phsys fa fa-apple"></span>
			                                        <img src="img/mobiledownload/ios_QRcode.png" alt=""/>
			                                        <h4>iOS 手机扫一扫 <i className="mps-quiz"></i></h4>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
				</div>
			);			
		case "Deposit":
			return(
				<div className="maincontent">
		            <div className="header-main-area black-soft sticky mCCBar">
		                <div className="container">
		                    <div className="row align-items-center position-relative">
		                        <div className="col-auto position-static">
		                            <div className="main-menu-area">
		                                <div className="main-menu">
		                                    <nav className="desktop-menu">
		                                        <ul>
		                                            <li onClick={() => props.chooseContent(props.category.accountInfo.name)}><div><i className="fa fa-id-card"></i>我的帐户</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.deposit.name)}><div><i className="fa fa-credit-card"></i>充值</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.withdraw.name)}><div><i className="fa fa-money"></i>提款</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.gameHistory.name)}><div><i className="fa fa-history"></i>游戏记录</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.financialHistory.name)}><div><i className="fa fa-clipboard"></i>财务记录</div></li>
		                                        </ul>
		                                    </nav>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>						
			        <div className="checkout-page-wrapper section-padding acc">
			            <div className="container acc">
			                <h5 className="actitile">存款方式选项</h5>
			                <div className="row">
			                    <div className="col-lg-3">
			                        <div className="blog-post-item white-container option-item">
			                            <section className="section-block-option deposit alipay">
			                                <h3>支付宝</h3><small>支付宝</small>
			                            </section>
			                        </div>
			                    </div>
			                    <div className="col-lg-3">
			                        <div className="blog-post-item white-container option-item">
			                            <section className="section-block-option deposit qpass">
			                                <h3>云闪付</h3><small>云闪付</small>
			                            </section>
			                        </div>
			                    </div>
			                    <div className="col-lg-3">
			                        <div className="blog-post-item white-container option-item">
			                            <section className="section-block-option deposit corp">
			                                <h3>线下入款</h3><small>线下入款</small>
			                            </section>
			                        </div>
			                    </div>
			                    <div className="col-lg-3">
			                        <div className="blog-post-item white-container option-item">
			                            <section className="section-block-option deposit online">
			                                <h3>在线 / 手机支付</h3><small>在线支付</small>
			                            </section>
			                        </div>
			                    </div>
			                    <div className="col-lg-12">
			                        <div className="checkout-billing-details-wrap blog-post-item s2">
			                            <h5 className="checkout-title">最近存款</h5>
			                            <div className="cart-table table-responsive table-responsive2 acbox1">
			                                <table className="table table-bordered">
			                                    <thead>
			                                        <tr>
			                                            <th>交易日期</th>
			                                            <th>支付类型</th>
			                                            <th>存款金额</th>
			                                            <th>订单号</th>
			                                            <th>状态</th>
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                                        <tr>
			                                            <td>2019-1129 12:08:08</td>
			                                            <td>网银转帐</td>
			                                            <td>8888.88</td>
			                                            <td>6937fbe1-04f6-423a-8c3d-f851f6499336</td>
			                                            <td>成功</td>
			                                        </tr>
			                                        <tr>
			                                            <td>2019-1129 12:08:08</td>
			                                            <td>网银转帐</td>
			                                            <td>8888.88</td>
			                                            <td>6937fbe1-04f6-423a-8c3d-f851f6499336</td>
			                                            <td>成功</td>
			                                        </tr>
			                                    </tbody>
			                                </table>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>   				
			);
		case "Withdraw":
			return(
				<div className="maincontent">
		            <div className="header-main-area black-soft sticky mCCBar">
		                <div className="container">
		                    <div className="row align-items-center position-relative">
		                        <div className="col-auto position-static">
		                            <div className="main-menu-area">
		                                <div className="main-menu">
		                                    <nav className="desktop-menu">
		                                        <ul>
		                                            <li onClick={() => props.chooseContent(props.category.accountInfo.name)}><div><i className="fa fa-id-card"></i>我的帐户</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.deposit.name)}><div><i className="fa fa-credit-card"></i>充值</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.withdraw.name)}><div><i className="fa fa-money"></i>提款</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.gameHistory.name)}><div><i className="fa fa-history"></i>游戏记录</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.financialHistory.name)}><div><i className="fa fa-clipboard"></i>财务记录</div></li>
		                                        </ul>
		                                    </nav>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>					
			        <div className="checkout-page-wrapper section-padding acc">
			            <div className="container acc">
			                <div className="row">

			                    <div className="col-lg-12">
			                        <div className="checkout-billing-details-wrap blog-post-item s2">
			                            <h5 className="checkout-title">稽核状态</h5>
			                            <div className="cart-table table-responsive table-responsive2 acbox2">
			                                <table className="table table-bordered">
			                                    <thead>
			                                        <tr>
			                                            <th>时间</th>
			                                            <th>稽核<br></br>种类</th>
			                                            <th>存款<br></br>金额</th>
			                                            <th>优惠金</th>
			                                            <th>有效投注</th>
			                                            <th>有效<br></br>投注稽核</th>
			                                            <th>状态</th>
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                                        <tr>
			                                            <td>2019-12-03 09:00:08<br></br>2019-12-04 14:25:00</td>
			                                            <td>常态<br></br>稽核</td>
			                                            <td>88888</td>
			                                            <td>88</td>
			                                            <td>19,738.73</td>
			                                            <td>555.00</td>
			                                            <td>通过</td>
			                                        </tr>
			                                        <tr>
			                                            <td>2019-12-03 09:00:08<br></br>2019-12-04 14:25:00</td>
			                                            <td>常态<br></br>稽核</td>
			                                            <td>88888</td>
			                                            <td>88</td>
			                                            <td>19,738.73</td>
			                                            <td>555.00</td>
			                                            <td>通过</td>
			                                        </tr>
			                                    </tbody>
			                                </table>
			                            </div>
			                        </div>
			                    </div>

			                    <div className="col-lg-6">
			                        <div className="blog-post-item white-container option-item s2">
			                            <h5 className="checkout-title">罚金</h5>
			                            <div className="boxes">
			                                <div className="promofee">
			                                    <div>
			                                        <button title="" className="nrc-button btn-main false">未达优惠稽核</button>
			                                    </div>
			                                    <h4 className="text-center">优惠稽核</h4>
			                                    <div className="total">
			                                        <p>总扣除</p>
			                                        <p>0.00</p>
			                                    </div>
			                                </div>
			                                <div className="generalfee">
			                                    <div>
			                                        <button title="" className="nrc-button btn-main false">未达常态稽核</button>
			                                    </div>
			                                    <h4 className="text-center">常态稽核</h4>
			                                    <div className="total">
			                                        <p>总扣除</p>
			                                        <p>0.00</p>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                    <div className="col-lg-6">
			                        <div className="blog-post-item white-container option-item s2">
			                            <h5 className="checkout-title">提款</h5>
			                            <form className="nrc-form nrc-form-block">
			                                <div className="my-main-wallet">
			                                    <p>可提款金额</p>
			                                    <p>26,830.29</p>
			                                </div>
			                                <div className="nrc-form-item ">
			                                    <div className="nrc-form-label ">
			                                        <label className="form-require">提款金额</label>
			                                    </div>
			                                    <div className="nrc-form-input nrc-u-1-1 ">
			                                        <input type="text" id="withdrawalamt" className=""/><small className="info-msg"></small>
			                                    </div>
			                                </div>
			                                <div className="nrc-form-item"><div></div><small>总扣除 0.00</small></div>
			                                <div className="actual-withdrawal">
			                                    <p>实际提领
			                                        <br></br><span></span></p>
			                                    <p>0.00</p>
			                                </div>
			                                <div style={{"textAlign": "right"}}>
			                                    <p></p>
			                                </div>
			                                <div className="flex-center">
			                                    <button className="nrc-button nrc-button-disabled">提款</button>
			                                </div>
			                            </form>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>      				
			);
		case "GameHistory":
			return(
				<div className="maincontent">
		            <div className="header-main-area black-soft sticky mCCBar">
		                <div className="container">
		                    <div className="row align-items-center position-relative">
		                        <div className="col-auto position-static">
		                            <div className="main-menu-area">
		                                <div className="main-menu">
		                                    <nav className="desktop-menu">
		                                        <ul>
		                                            <li onClick={() => props.chooseContent(props.category.accountInfo.name)}><div><i className="fa fa-id-card"></i>我的帐户</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.deposit.name)}><div><i className="fa fa-credit-card"></i>充值</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.withdraw.name)}><div><i className="fa fa-money"></i>提款</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.gameHistory.name)}><div><i className="fa fa-history"></i>游戏记录</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.financialHistory.name)}><div><i className="fa fa-clipboard"></i>财务记录</div></li>
		                                        </ul>
		                                    </nav>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
					<div className="container">
						<div className="row">
							<div className="col-md-12 hm">				
							<div className=" blog-post-item">
								<div className="col-lg-12">
									<div className="product-review-info">
										<h5 className="checkout-title">游戏记录<small>*存取数据时可能会延迟，若未找到游戏纪录，请稍后重试</small></h5>										
										<div className="tab-content reviews-tab">
											<form className="form_area1">
												<div className="col-lg-12 from_unit s2">
												<div className="select sB">
													<select name='' value={props.category.register.currency} onChange={props.handleChange}>
														<option value="">总览</option>
														<option value="">真人娱乐</option>
														<option value="">体育赛事</option>
													</select>
												</div>
													<label>时间区间:</label>
													<input className="dateS" type="text" id="datepicker"/>&nbsp;-&nbsp;
													<input className="dateS"type="text" id="datepicker2"/>&nbsp;&nbsp;
													<button title="" className="nrc-button">查询</button>
												</div>			                                            
											</form>
											<div className="tab-pane fade show active" id="tab_one">
												<div className="cart-table table-responsive table-responsive2 acbox4">
													<table className="table table-bordered">
														<thead>
															<tr className="Fline">
																<th>结算日期</th>
																<th>笔数</th>
																<th>总投注</th>
																<th>有效投注</th>
																<th>输/赢</th> 
															</tr>
														</thead>
														<tbody>
															<tr className="Fline">
																<td>2019-11-29<br></br>星期五</td>
																<td>2</td>
																<td>1,271.00</td>
																<td>1,249.00</td>
																<td>888.80</td>
															</tr>
															<tr>
																<td colSpan="5">
																<table className="table table-bordered s2">
																	<thead>
																		<tr>
																			<th>游戏</th>
																			<th>下注金额</th>
																			<th>结果</th>
																			<th>下注时间</th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<td>视讯百家</td>
																			<td>100</td>
																			<td>95.0</td>
																			<td>2019-11-29<br></br>16:45:55</td>
																		</tr>
																		<tr>
																			<td>视讯百家</td>
																			<td>100</td>
																			<td>95.0</td>
																			<td>2019-11-29<br></br>16:45:55</td>
																		</tr>
																	</tbody>
																</table>
																</td>
															</tr>
															<tr>
																<td>2019-11-29<br></br>星期五</td>
																<td>8</td>
																<td>1,271.00</td>
																<td>1,249.00</td>
																<td>888.80</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
											<div className="tab-pane fade" id="tab_two">

												<form className="form_area1">
													<div className="col-lg-1 from_unit"></div>
													<div className="col-lg-5 from_unit">
														<span>游戏商:</span>
														<select name="">
															<option>--所有游戏商--</option>
															<option value="">游戏商1</option>
															<option value="">游戏商2</option>
														</select>
													</div>
													<div className="col-lg-5 from_unit">
														<label>游戏:</label>
														<select name="">
															<option>--所有游戏--</option>
															<option value="">游戏1</option>
															<option value="">游戏2</option>
														</select>
													</div>
													<div className="col-lg-1 from_unit"></div>
													<div className="col-lg-5 from_unit">
													123
													</div>
													<div className="col-lg-5 from_unit">
													<label>游戏:</label>
														<select name="">
															<option>--所有游戏--</option>
															<option value="">游戏1</option>
															<option value="">游戏2</option>
														</select>
													</div>
													<div className="col-lg-2 from_unit">
														<button title="" className="nrc-button">查询</button>
													</div>
												</form>

												<div className="cart-table table-responsive">
													<table className="table table-bordered">
														<thead>
															<tr>
																<th>投注时间</th>
																<th>结算时间</th>
																<th>注单号</th>
																<th>游戏商</th>
																<th>游戏</th>
																<th>总投注</th>
																<th>有效投注</th>
																<th>输/赢</th>
																<th>状态</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>2019-11-29 星期五</td>
																<td>已结算</td>
																<td>8</td>
																<td>1,271.00</td>
																<td>1,249.00</td>
																<td>888.80</td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>	
							</div>
						</div>
					</div>
				</div>			
			);
		case "FinancialHistory":
			return(
				<div className="maincontent">
		            <div className="header-main-area black-soft sticky mCCBar">
		                <div className="container">
		                    <div className="row align-items-center position-relative">
		                        <div className="col-auto position-static">
		                            <div className="main-menu-area">
		                                <div className="main-menu">
		                                    <nav className="desktop-menu">
		                                        <ul>
		                                            <li onClick={() => props.chooseContent(props.category.accountInfo.name)}><div><i className="fa fa-id-card"></i>我的帐户</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.deposit.name)}><div><i className="fa fa-credit-card"></i>充值</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.withdraw.name)}><div><i className="fa fa-money"></i>提款</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.gameHistory.name)}><div><i className="fa fa-history"></i>游戏记录</div></li>
		                                            <li onClick={() => props.chooseContent(props.category.financialHistory.name)}><div><i className="fa fa-clipboard"></i>财务记录</div></li>
		                                        </ul>
		                                    </nav>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>				
			        <div className="container">
			            <div className="row">
			                <div className="col-md-12 hm">
			                    <div className=" blog-post-item">
			                        <div className="col-lg-12">
			                            <div className="product-review-info">
			                                <h5 className="checkout-title">财务记录</h5>
			                                <div className="tab-content reviews-tab">
			                                    <div className="tab-pane fade show active" id="tab_one">
			                                        <form className="form_area1">
			                                            <div className="col-lg-12 from_unit s2">
															<div className="select sB">
																<select name='' value={props.category.register.currency} onChange={props.handleChange}>
																	<option value="">財務总览</option>
																	<option value="">存款记录</option>
																	<option value="">提款记录</option>
																	<option value="">优惠记录</option>
																	<option value="">返水记录</option>
																	<option value="">手动调整记录</option>
																	<option value="">推荐记录</option>
																</select>
															</div>
			                                              	<label>时间区间:</label>
			                                            	<input className="dateS" type="text" id="datepicker"/>&nbsp;-&nbsp;
			                                                <input className="dateS"type="text" id="datepicker2"/>&nbsp;&nbsp;
															<button title="" className="nrc-button">查询</button>
			                                            </div>			                                            
			                                        </form>
			                                        <div className="cart-table table-responsive table-responsive2 acbox3">
			                                            <table className="table table-bordered">
			                                                <thead>
			                                                    <tr>
			                                                        <th>交易日期</th>
			                                                        <th>交易类型</th>
			                                                        <th>交易金額</th>
			                                                        <th>订单号</th>
			                                                        <th>状态</th>
			                                                    </tr>
			                                                </thead>
			                                                <tbody>
			                                                    <tr>
			                                                        <td>2019-11-29<br></br>星期五</td>
			                                                        <td>交易类型</td>
			                                                        <td>888888888</td>
			                                                        <td>0000001</td>
			                                                        <td>状态</td>
			                                                    </tr>
			                                                </tbody>
			                                            </table>
			                                        </div>
			                                    </div>
			                                    <div className="tab-pane fade show" id="tab_two">
			                                        <form className="form_area1">
			                                            <div className="col-lg-3 from_unit"></div>
			                                            <div className="col-lg-5 from_unit">
			                                              <label>时间区间:</label>
			                                                <input className="dateS" type="text" id="datepickerB1"/>
			                                                <input className="dateS"type="text" id="datepickerB2"/>
			                                            </div>
			                                            <div className="col-lg-3 from_unit">
			                                                <button title="" className="nrc-button">查询</button>
			                                            </div>
			                                            <div className="col-lg-1 from_unit"></div>
			                                        </form>
			                                        <div className="cart-table table-responsive">
			                                            <table className="table table-bordered">
			                                                <thead>
			                                                    <tr>
			                                                        <th>存款时间</th>
			                                                        <th>存款方式</th>
			                                                        <th>存款金额</th>
			                                                        <th>订单号</th>
			                                                        <th>状态</th>
			                                                    </tr>
			                                                </thead>
			                                                <tbody>
			                                                    <tr>
			                                                        <td>2019-11-29 星期五</td>
			                                                        <td>存款方式</td>
			                                                        <td>888888888</td>
			                                                        <td>0000001</td>
			                                                        <td>状态</td>
			                                                    </tr>
			                                                </tbody>
			                                            </table>
			                                        </div>
			                                    </div>
			                                    <div className="tab-pane fade show" id="tab_three">
			                                        <form className="form_area1">
			                                            <div className="col-lg-3 from_unit"></div>
			                                            <div className="col-lg-5 from_unit">
			                                              <label>时间区间:</label>
			                                                <input className="dateS" type="text" id="datepickerC1"/>&nbsp;-&nbsp;
			                                                <input className="dateS"type="text" id="datepickerC2"/>
			                                            </div>
			                                            <div className="col-lg-3 from_unit">
			                                                <button title="" className="nrc-button">查询</button>
			                                            </div>
			                                            <div className="col-lg-1 from_unit"></div>
			                                        </form>
			                                        <div className="cart-table table-responsive">
			                                            <table className="table table-bordered">
			                                                <thead>
			                                                    <tr>
			                                                        <th>提款日期</th>
			                                                        <th>银行</th>
			                                                        <th>提款金额</th>
			                                                        <th>实际提领</th>
			                                                        <th>银行卡号</th>
			                                                        <th>状态</th>
			                                                    </tr>
			                                                </thead>
			                                                <tbody>
			                                                    <tr>
			                                                        <td>2019-11-29 星期五</td>
			                                                        <td>银行</td>
			                                                        <td>888888888</td>
			                                                        <td>888888888</td>
			                                                        <td>4585-85758-9588-8888</td>
			                                                        <td>状态</td>
			                                                    </tr>
			                                                </tbody>
			                                            </table>
			                                        </div>
			                                    </div>
			                                    <div className="tab-pane fade show" id="tab_four">
			                                        <form className="form_area1">
			                                            <div className="col-lg-3 from_unit"></div>
			                                            <div className="col-lg-5 from_unit">
			                                              <label>时间区间:</label>
			                                                <input className="dateS" type="text" id="datepickerD1"/>&nbsp;-&nbsp;
			                                                <input className="dateS"type="text" id="datepickerD2"/>
			                                            </div>
			                                            <div className="col-lg-3 from_unit">
			                                                <button title="" className="nrc-button">查询</button>
			                                            </div>
			                                            <div className="col-lg-1 from_unit"></div>
			                                        </form>
			                                        <div className="cart-table table-responsive">
			                                            <table className="table table-bordered">
			                                                <thead>
			                                                    <tr>
			                                                        <th>申请时间</th>
			                                                        <th>优惠类型</th>
			                                                        <th>优惠名称</th>
			                                                        <th>优惠金额</th>
			                                                        <th>状态</th>
			                                                    </tr>
			                                                </thead>
			                                                <tbody>
			                                                    <tr>
			                                                        <td>2019-11-29 星期五</td>
			                                                        <td>优惠类型</td>
			                                                        <td>优惠名称</td>
			                                                        <td>888888888</td>
			                                                        <td>状态</td>
			                                                    </tr>
			                                                </tbody>
			                                            </table>
			                                        </div>
			                                    </div>
			                                    <div className="tab-pane fade show" id="tab_five">
			                                        <form className="form_area1">
			                                            <div className="col-lg-3 from_unit"></div>
			                                            <div className="col-lg-5 from_unit">
			                                              <label>时间区间:</label>
			                                                <input className="dateS" type="text" id="datepickerE1"/>
			                                                <input className="dateS"type="text" id="datepickerE2"/>
			                                            </div>
			                                            <div className="col-lg-3 from_unit">
			                                                <button title="" className="nrc-button">查询</button>
			                                            </div>
			                                            <div className="col-lg-1 from_unit"></div>
			                                        </form>
			                                        <div className="cart-table table-responsive">
			                                            <table className="table table-bordered">
			                                                <thead>
			                                                    <tr>
			                                                        <th>返水日期</th>
			                                                        <th>返水类型</th>
			                                                        <th>返水名称</th>
			                                                        <th>返水额</th>
			                                                        <th>订单号</th>
			                                                        <th>状态</th>
			                                                    </tr>
			                                                </thead>
			                                                <tbody>
			                                                    <tr>
			                                                        <td>2019-11-29 星期五</td>
			                                                        <td>返水类型</td>
			                                                        <td>返水名称</td>
			                                                        <td>888888888</td>
			                                                        <td>0000001</td>
			                                                        <td>状态</td>
			                                                    </tr>
			                                                </tbody>
			                                            </table>
			                                        </div>
			                                    </div>
			                                    <div className="tab-pane fade show" id="tab_six">
			                                        <form className="form_area1">
			                                            <div className="col-lg-3 from_unit"></div>
			                                            <div className="col-lg-5 from_unit">
			                                              <label>时间区间:</label>
			                                                <input className="dateS" type="text" id="datepickerF1"/>
			                                                <input className="dateS"type="text" id="datepickerF2"/>
			                                            </div>
			                                            <div className="col-lg-3 from_unit">
			                                                <button title="" className="nrc-button">查询</button>
			                                            </div>
			                                            <div className="col-lg-1 from_unit"></div>
			                                        </form>
			                                        <div className="cart-table table-responsive">
			                                            <table className="table table-bordered">
			                                                <thead>
			                                                    <tr>
			                                                        <th>调整日期</th>
			                                                        <th>调整类型</th>
			                                                        <th>钱包</th>
			                                                        <th>调整金额</th>
			                                                    </tr>
			                                                </thead>
			                                                <tbody>
			                                                    <tr>
			                                                        <td>2019-11-29 星期五</td>
			                                                        <td>调整类型</td>
			                                                        <td>钱包</td>
			                                                        <td>888888888</td>
			                                                    </tr>
			                                                </tbody>
			                                            </table>
			                                        </div>
			                                    </div>
			                                    <div className="tab-pane fade show" id="tab_seven">
			                                        <form className="form_area1">
			                                            <div className="col-lg-3 from_unit"></div>
			                                            <div className="col-lg-5 from_unit">
			                                              <label>时间区间:</label>
			                                                <input className="dateS" type="text" id="datepickerG1"/>
			                                                <input className="dateS"type="text" id="datepickerG2"/>
			                                            </div>
			                                            <div className="col-lg-3 from_unit">
			                                                <button title="" className="nrc-button">查询</button>
			                                            </div>
			                                            <div className="col-lg-1 from_unit"></div>
			                                        </form>
			                                        <div className="cart-table table-responsive">
			                                            <table className="table table-bordered">
			                                                <thead>
			                                                    <tr>
			                                                        <th>交易日期</th>
			                                                        <th>交易类型</th>
			                                                        <th>交易金额</th>
			                                                        <th>状态</th>
			                                                    </tr>
			                                                </thead>
			                                                <tbody>
			                                                    <tr>
			                                                        <td>2019-11-29 星期五</td>
			                                                        <td>交易类型</td>
			                                                        <td>8888888888888</td>
			                                                        <td>状态</td>
			                                                    </tr>
			                                                </tbody>
			                                            </table>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
                </div>			
			);
		case "About":
			return(
				<div className="maincontent">
			        <div className="my-account-wrapper section-padding">
			            <div className="container">
			                <div className="section-bg-color">
			                    <div className="row">
			                        <div className="col-lg-12">
			                            <div className="myaccount-page-wrapper">
			                                <div className="row">
			                                    <div className="col-lg-3 col-md-4 col-sm-12 pcAbout">
			                                        <div className="myaccount-tab-menu nav" role="tablist">
														<div className={props.currentSubContent==="About"?"active":""} 
															 onClick={()=> props.chooseSubContent("About")}>
															<i className="fa fa-angle-double-right"></i>
															关于 WM Casino
														</div> 
														<div className={props.currentSubContent==="Agreement"?"active":""} 
															 onClick={()=> props.chooseSubContent("Agreement")}>
															<i className="fa fa-angle-double-right"></i>
															用户协议
														</div> 
														<div className={props.currentSubContent==="ReasonableGaming"?"active":""} 
															 onClick={()=> props.chooseSubContent("ReasonableGaming")}>
															<i className="fa fa-angle-double-right"></i>
															理性博彩
														</div> 	
														<div className={props.currentSubContent==="Disclaimer"?"active":""} 
															 onClick={()=> props.chooseSubContent("Disclaimer")}>
															<i className="fa fa-angle-double-right"></i>
															免责声明
														</div> 
														<div className={props.currentSubContent==="PersonalPrivacy"?"active":""} 
															 onClick={()=> props.chooseSubContent("PersonalPrivacy")}>
															<i className="fa fa-angle-double-right"></i>
															用户隐私
														</div> 
														<div className={props.currentSubContent==="DepositSupport"?"active":""}
															 onClick={()=> props.chooseSubContent("DepositSupport")}>
															<i className="fa fa-angle-double-right"></i>
															存款帮助
														</div> 		
														<div className={props.currentSubContent==="WithdrawSupport"?"active":""} 
															 onClick={()=> props.chooseSubContent("WithdrawSupport")}>
															<i className="fa fa-angle-double-right"></i>
															取款帮助
														</div> 
														<div className={props.currentSubContent==="FAQ"?"active":""} 
															 onClick={()=> props.chooseSubContent("FAQ")}>
															<i className="fa fa-angle-double-right"></i>
															常见问题
														</div> 
														<div className={props.currentSubContent==="ContactUs"?"active":""} 
															 onClick={()=> props.chooseSubContent("ContactUs")}>
															<i className="fa fa-angle-double-right"></i>
															联系我们
														</div> 																																																									                                       		      			                                            		                                            			                                            			                                            			                                            
			                                        </div>
			                                    </div>
												<div className="col-sm-12 mobileAbout">
													<div className="select sC">
														<select value={props.category.about.currentMainContent} onChange={props.chooseSubContent_mobile}>
															<option value="About">关于 WM Casino</option>
															<option value="Agreement">用户协议</option>
															<option value="ReasonableGaming">理性博彩</option>
															<option value="Disclaimer">免责声明</option>
															<option value="PersonalPrivacy">用户隐私</option>
															<option value="DepositSupport">存款帮助</option>
															<option value="WithdrawSupport">取款帮助</option>
															<option value="FAQ">常见问题</option>
															<option value="ContactUs">联系我们</option>
														</select>
													</div>
												</div>
			                                    <div className="col-lg-9 col-md-8 col-sm-12">
			                                        <div className="tab-content">
			                                            <div className="tab-pane fade show active">
			                                            	<SubContent
			                                            		category={props.currentSubContent}
			                                            	/>
			                                            </div>
			                                        </div>
			                                    </div> 
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
				</div>			
			);
		default:
			break;	
	}
}


export {MainContent};