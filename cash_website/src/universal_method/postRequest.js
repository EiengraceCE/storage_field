function encodeFormData(data){
	console.log("doing encode");
	if (!data) return "";   // Always return a string
	var pairs = [];         // To hold name=value pairs
	for (var name in data) {                                    // For each name
		if (!data.hasOwnProperty(name)) continue;               // Skip inherited
		if (typeof data[name] === "function") continue;         // Skip methods
		if (!data[name]) continue;
		var value = data[name].toString();                      // Value as string
		name = encodeURIComponent(name/*.replace(" ", "+")*/);      // Encode name
		value = encodeURIComponent(value/*.replace(" ", "+")*/);    // Encode value
		pairs.push(name + "=" + value);     // Remember name=value pair
	}
	return pairs.join('&'); // Return joined pairs separated with &
}	
function postRequest(url, payload, onLoadFunc, onErrorFunc, bRetry){
	var xhr = new XMLHttpRequest();
	xhr.open("POST", url);
	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=UTF-8');
	xhr.onload = function(event)
	{
		var jsonData = null;
		try
		{
			jsonData = JSON.parse(xhr.responseText);
			if (onLoadFunc)
				onLoadFunc(jsonData);
		}
		catch(e)
		{
			console.log(e);
			console.log("xhr json error    bRetry: " + bRetry + "    data: \n" + xhr.responseText);
			if (bRetry)
			{
				setTimeout(function()
				{
					this.postRequest(url, payload, onLoadFunc, onErrorFunc, bRetry);
				}, 3000);
			}
		}
	};
	xhr.onerror = function(event)
	{
		console.log("xhr onerror    bRetry: " + bRetry);
		if (onErrorFunc)
			onErrorFunc(event);
		if (bRetry)
		{
			setTimeout(function()
			{
				this.postRequest(url, payload, onLoadFunc, onErrorFunc, bRetry);
			}, 3000);
		}
	};
	var encodedData = encodeFormData(payload);
	xhr.send(encodedData);
}	


export {postRequest}