import React from 'react';
import {InputItem} from './component/InputItem';
import {postRequest} from './universal_method/postRequest';
class Navigation extends React.Component{
	state={
		category:{
			register:
			{
				name:"Register"
			},
			account:{
				name:"Account"
			}				
		},
		account:'',
		password:''
	};
	handleChange = this.handleChange.bind(this);
	handleSubmit = this.handleSubmit.bind(this);	
	handleChange(event){
		const target=event.target;
		this.setState({
			[target.name]: target.value
		});
		// console.log(this.state[target.name]);
	}	
	handleSubmit(event){
		event.preventDefault();
		var url =  'https://api-01.a45.me/api/web/Gateway.php';
        var request = {
            cmd: 'GetSid',
            user:this.state.account,
            password:this.state.password
		};		
		postRequest(url, request ,(response) => this.props.getSidFunc(response));
		this.setState({
			account:'',
			password:''
		});
	}
	//--------------------render------------------------
	renderNav(category){
		switch(category){
			case "Classic":
				// console.log("ClassicNav");
				return (
					<div>
				        <div className="main-header d-none d-lg-block">
				            <div className="header-top black-bg">
				                <div className="container">
				                    <div className="row align-items-center">
				                        <div className="col-lg-12 d-flex justify-content-end">
				                            <ul className="user-info-block">
				                            {this.props.accountInfo.account?
				                            	<li onClick={() => {this.props.chooseContent(this.state.category.account.name); this.props.chooseNav(this.state.category.account.name)}}>
					                                <div>
					                                	<i className="fa fa-user-circle"></i> 
					                                	我的帐户
					                                </div>
				                                </li>:''
				                            }
				                            	<li></li>
				                                <li>
				                                    <div className="top-left-navigation">
				                                        <ul className="nav align-items-center">
				                                            <li className="language">
				                                                <i className="fa fa-sign-in"></i> {this.props.username?this.props.username:'登入'}
				                                                {this.props.accountInfo.sid?'':
					                                                <ul className="dropdown-list LoginArea">
					                                                    <li>
						                                                    <form onSubmit={this.handleSubmit}>
						                                                        <h4>登录 / Sign In</h4>
						                                                        {InputItem("text","请输入信箱或会员账号","account",this.state.account,(event) => this.handleChange(event),true)}
						                                                        {InputItem("password","请输入密码","password",this.state.password,(event) => this.handleChange(event),true)}						                                		                                                        
						                                                        <div className="single-input-item">
						                                                            <div className="login-reg-form-meta d-flex align-items-center justify-content-between">
						                                                                <div className="remember-meta">
						                                                                    <div className="custom-control custom-checkbox">
						                                                        				{InputItem("checkbox",undefined,undefined,undefined,undefined,undefined,undefined,'記住我')}						                                		                                                        						                                                                    	
						                                                                    </div>
						                                                                </div>
						                                                                <span className="forget-pwd">忘记密码?</span>
						                                                            </div>
						                                                        </div>
						                                                        <div className="single-input-item">
						                                                            <button type="submit" className="btn btn-sqr">登录</button>
						                                                        </div>
						                                                    </form>
					                                                    </li>
					                                                </ul>
				                                            	}

				                                            </li>
				                                        </ul>
				                                    </div>
				                                </li>
				                                <li>
				                                {this.props.accountInfo.sid?
				                                	<div onClick={() => this.props.logOut()}>登出</div>
				                                	:	
				                                	<div onClick={() => this.props.chooseContent(this.state.category.register.name)}>
				                                		<i className="fa fa-id-card"></i> 
				                                		免费注册
				                                	</div>
				                                }
				                                </li>
				                            </ul>
				                            <div className="top-left-navigation">
				                                <ul className="nav align-items-center">
				                                    <li className="language">
				                                        <img src="img/icon/cn.png" alt=""/> 简体中文 <i className="fa fa-angle-down"></i>
				                                        <ul className="dropdown-list">
				                                            <li><div ><img src="img/icon/cn.png" alt=""/> 简体中文</div></li>
				                                            <li><div ><img src="img/icon/tn.png" alt=""/> 繁體中文</div></li>
				                                            <li><div ><img src="img/icon/en.png" alt=""/> English</div></li>
				                                        </ul>
				                                    </li>
				                                </ul>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				            </div>
				            <div className="header-main-area black-soft sticky" >
				                <div className="container">
				                    <div className="row align-items-center position-relative">
				                        <div className="col-auto">
				                            <div className="logo" onClick={() => {this.props.chooseContent("Home"); this.props.chooseNav("Classic")}}>
				                                <div>
				                                    <img src="img/logo/logo.png" alt=""/>
				                                </div>
				                            </div>
				                        </div>
				                        <div className="col-auto position-static">
				                            <div className="main-menu-area">
				                                <div className="main-menu">
				                                    <nav className="desktop-menu">
				                                        <ul>
				                                            <li><div >优惠活动</div></li>
				                                            <li className="position-static"><div >电子竞技 <i className="fa fa-angle-down"></i></div>
				                                                <ul className="megamenu dropdown">
				                                                    <li className="mega-title">
				                                                        <div className="submenuIMG m21"></div>
				                                                    </li>
				                                                    <li className="mega-title">
				                                                        <div className="submenuIMG m22"></div>
				                                                    </li>
				                                                </ul>
				                                            </li>
				                                            <li className="position-static"><div >彩票游戏 <i className="fa fa-angle-down"></i></div>
				                                                <ul className="megamenu dropdown">
				                                                    <li className="mega-title"><div className="submenuIMG m31"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m32"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m33"></div></li>
				                                                </ul>
				                                            </li>
				                                            <li className="position-static">
				                                            	<div 
				                                            		onClick={this.props.accountInfo.sid?() => this.props.prepareOpenGame():() => this.props.chooseContent(this.state.category.register.name)}>真人娱乐 
				                                            	</div>
				                                            </li>
				                                            <li className="position-static"><div >电子游艺 <i className="fa fa-angle-down"></i></div>
				                                                <ul className="megamenu dropdown">
				                                                    <li className="mega-title"><div className="submenuIMG m51"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m52"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m53"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m54"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m55"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m56"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m57"></div></li>
				                                                </ul>
				                                            </li>
				                                            <li className="position-static"><div >体育赛事 <i className="fa fa-angle-down"></i></div>
				                                                <ul className="megamenu dropdown">
				                                                    <li className="mega-title"><div className="submenuIMG m61"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m62"></div></li>
				                                                </ul>
				                                            </li>
				                                            <li className="position-static"><div >捕鱼游戏 <i className="fa fa-angle-down"></i></div>
				                                                <ul className="megamenu dropdown">
				                                                    <li className="mega-title"><div className="submenuIMG m71"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m72"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m73"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m74"></div></li>
				                                                </ul>
				                                            </li>
				                                            <li className="position-static"><div>动物竞技 <i className="fa fa-angle-down"></i></div>
				                                                <ul className="megamenu dropdown">
				                                                    <li className="mega-title"><div className="submenuIMG m81"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m82"></div></li>
				                                                </ul>
				                                            </li>
				                                            <li className="position-static"><div>棋牌 <i className="fa fa-angle-down"></i></div>
				                                                <ul className="megamenu dropdown">
				                                                    <li className="mega-title"><div className="submenuIMG m91"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m92"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m93"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m94"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m95"></div></li>
				                                                    <li className="mega-title"><div className="submenuIMG m96"></div></li>
				                                                </ul>
				                                            </li>
				                                            <li><div>手机下载</div></li>
				                                        </ul>
				                                    </nav>
				                                </div>
				                            </div>
				                        </div>
				                        <div className="col-auto ml-auto">
				                            <div className="header-right">
				                                <div className="header-configure-area">
				                                    <ul className="nav">
				                                        <li>
				                                            <div className="search-trigger"><i className="fa fa-search"></i></div>
				                                        </li>
				                                    </ul>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				        <div className="mobile-header d-lg-none d-md-block sticky black-soft">
				            <div className="container-fluid">
				                <div className="row align-items-center">
				                    <div className="col-12">
				                        <div className="mobile-main-header">
				                            <div className="mobile-logo" onClick={() => {this.props.chooseContent("Home"); this.props.chooseNav("Classic")}}>
				                                <div>
				                                    <img src="img/logo/logo.png" alt=""/>
				                                </div>
				                            </div>
				                            <div className="mobile-menu-toggler">
				                                <button className="mobile-menu-btn">
				                                    <span></span>
				                                    <span></span>
				                                    <span></span>
				                                </button>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				        <aside className="off-canvas-wrapper">
				            <div className="off-canvas-overlay"></div>
				            <div className="off-canvas-inner-content">
				                <div className="btn-close-off-canvas">
				                    <i className="fa fa-close"></i>
				                </div>

				                <div className="off-canvas-inner">
				                    <div className="search-box-offcanvas">
				                        <form>
				                            <input type="text" placeholder="请再此处搜寻......"/>
				                            <button className="search-btn"><i className="fa fa-search"></i></button>
				                        </form>
				                    </div>
				                    <div className="mobile-navigation">
				                        <nav>
				                            <ul className="mobile-menu">
				                                <li><div>优惠活动</div></li>
				                                <li className="menu-item-has-children "><div>电子竞技</div>
				                                    <ul className="dropdown">
				                                        <li><div>AE电竞</div></li>
				                                        <li><div>电竞牛</div></li>
				                                    </ul>
				                                </li>
				                                <li className="menu-item-has-children "><div>彩票游戏</div>
				                                    <ul className="dropdown">
				                                        <li><div>IG彩票</div></li>
				                                        <li><div>诺应彩票</div></li>
				                                        <li><div>VR Gaming</div></li>
				                                    </ul>
				                                </li>
                                            	<div 
                                            		onClick={this.props.accountInfo.sid?() => this.props.prepareOpenGame():() => this.props.chooseContent(this.state.category.register.name)}>真人娱乐 
                                            	</div>
				                                <li className="menu-item-has-children "><div>电子游艺</div>
				                                    <ul className="dropdown">
				                                        <li><div>AMEBA</div></li>
				                                        <li><div>CQ9</div></li>
				                                        <li><div>JDB</div></li>
				                                        <li><div>Microgoming</div></li>
				                                        <li><div>Red Tiger</div></li>
				                                        <li><div>SA Gaming</div></li>
				                                        <li><div>Virtual Tech</div></li>
				                                    </ul>
				                                </li>
				                                <li className="menu-item-has-children "><div>体育赛事</div>
				                                    <ul className="dropdown">
				                                        <li><div>Saba Platform</div></li>
				                                        <li><div>United Gaming</div></li>
				                                    </ul>
				                                </li>
				                                <li className="menu-item-has-children "><div>捕鱼游戏</div>
				                                    <ul className="dropdown">
				                                        <li><div>CQ9</div></li>
				                                        <li><div>GG Gaming</div></li>
				                                        <li><div>JDB</div></li>
				                                        <li><div>大满贯</div></li>
				                                    </ul>
				                                </li>
				                                <li className="menu-item-has-children "><div>动物竞技</div>
				                                    <ul className="dropdown">
				                                        <li><div>SV388</div></li>
				                                        <li><div>TRC</div></li>
				                                    </ul>
				                                </li>
				                                <li className="menu-item-has-children "><div>棋牌</div>
				                                    <ul className="dropdown">
				                                        <li><div>AE棋牌</div></li>
				                                        <li><div>365棋牌</div></li>
				                                        <li><div>JDB</div></li>
				                                        <li><div>KY开元棋牌</div></li>
				                                        <li><div>龙城娱乐</div></li>
				                                        <li><div>棋牌娱乐</div></li>
				                                    </ul>
				                                </li>
				                                <li><div>手机下载</div></li>
				                            </ul>
				                        </nav>
				                    </div>
				                    <div className="mobile-settings">
				                        <ul className="nav">
				                            <li>
				                                <div className="dropdown mobile-top-dropdown">
				                                    <div className="dropdown-toggle" id="currency" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				                                        语言
				                                        <i className="fa fa-angle-down"></i>
				                                    </div>
				                                    <div className="dropdown-menu" aria-labelledby="currency">
				                                        <div className="dropdown-item">简体中文</div>
				                                        <div className="dropdown-item">繁體中文</div>
				                                        <div className="dropdown-item">English</div>
				                                    </div>
				                                </div>
				                            </li>
				                            <li>
				                                <div className="dropdown mobile-top-dropdown">
				                                    <div className="dropdown-toggle" id="myaccount" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				                                        我的帐户
				                                        <i className="fa fa-angle-down"></i>
				                                    </div>
				                                    <div className="dropdown-menu" aria-labelledby="myaccount">
				                                        <div className="dropdown-item"  target="_blank">我的帐户</div>
				                                        <div className="dropdown-item"  target="_blank">推荐好友</div>
				                                        <div className="dropdown-item"  target="_blank">充值</div>
				                                        <div className="dropdown-item"  target="_blank">提款</div>
				                                        <div className="dropdown-item"  target="_blank">游戏记录</div>
				                                        <div className="dropdown-item"  target="_blank">财务记录</div>
				                                        <div className="dropdown-item"  target="_blank">公告中心</div>
				                                    </div>
				                                </div>
				                            </li>
				                        </ul>
				                    </div>
				                    <div className="offcanvas-widget-area">
				                        <div className="off-canvas-contact-widget">
				                            <ul>
				                                <li><i className="fa fa-mobile"></i>
				                                    <div>+886 938 888888</div>
				                                </li>
				                                <li><i className="fa fa-envelope-o"></i>
				                                    <div>lexisbetman@gmail.com</div>
				                                </li>
				                            </ul>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </aside>
				    </div> 
				);
			case "Account":
				// console.log("AccountNav");
				return (
				    <div className="header-area">
				        <div className="main-header d-none d-lg-block">
				            <div className="header-top black-bg">
				                <div className="container">
				                    <div className="row align-items-center">
				                        <div className="col-lg-12 d-flex justify-content-end">
				                            <ul className="user-info-block">
				                                <li><div><i className="fa fa-user-circle"></i> {this.props.username}</div></li>
				                                <li><div className="login-trigger" 
				                                		 onClick={
				                                		 	() => {this.props.chooseContent("Home"); 
				                                		 	       this.props.chooseNav("Classic"); 
				                                		 	       this.props.logOut();}}
				                                	><i className="fa fa-sign-out"></i> 登出</div></li>
				                            </ul>
				                        </div>
				                    </div>
				                </div>
				            </div>
				            <div className="header-main-area black-soft accStyle sticky">
				                <div className="container">
				                    <div className="row align-items-center position-relative">
				                        <div className="col-auto">
				                            <div className="logo accStyle"  onClick={() => {this.props.chooseContent("Home"); this.props.chooseNav("Classic")}}>
				                                <div>
				                                    <img src="../img/logo/logo.png" alt=""/>
				                                </div>
				                            </div>
				                        </div>
				                        <div className="col-auto position-static">
				                            <div className="main-menu-area">
				                                <div className="main-menu">
				                                    <nav className="desktop-menu">
				                                        <ul>
				                                            <li><div><i className="fa fa-id-card"></i>我的帐户</div></li>
				                                            <li><div><i className="fa fa-share"></i>推荐好友</div></li>
				                                            <li><div><i className="fa fa-credit-card"></i>充值</div></li>
				                                            <li><div><i className="fa fa-money"></i>提款</div></li>
				                                            <li><div><i className="fa fa-history"></i>游戏记录</div></li>
				                                            <li><div><i className="fa fa-clipboard"></i>财务记录</div></li>
				                                            <li><div><i className="fa fa-bullhorn"></i>公告中心</div></li>
				                                        </ul>
				                                    </nav>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				        <div className="mobile-header d-lg-none d-md-block sticky black-soft">
				            <div className="container-fluid">
				                <div className="row align-items-center">
				                    <div className="col-12">
				                        <div className="mobile-main-header">
				                            <div className="mobile-logo">
				                                <div onClick={() => {this.props.chooseContent("Home"); this.props.chooseNav("Classic")}}>
				                                    <img src="../img/logo/logo.png" alt=""/>
				                                </div>
				                            </div>
				                            <div className="mobile-menu-toggler">
				                                <button className="mobile-menu-btn">
				                                    <span></span>
				                                    <span></span>
				                                    <span></span>
				                                </button>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				        <aside className="off-canvas-wrapper">
				            <div className="off-canvas-overlay"></div>
				            <div className="off-canvas-inner-content">
				                <div className="btn-close-off-canvas">
				                    <i className="fa fa-close"></i>
				                </div>

				                <div className="off-canvas-inner">
				                    <div className="search-box-offcanvas">
				                        <form>
				                            <input type="text" placeholder="请再此处搜寻......"/>
				                            <button className="search-btn"><i className="fa fa-search"></i></button>
				                        </form>
				                    </div>
				                    <div className="mobile-navigation">
				                        <nav>
				                            <ul className="mobile-menu">
				                                <li><div>我的帐户</div></li>
				                                <li><div>推荐好友</div></li>
				                                <li><div>充值</div></li>
				                                <li><div>提款</div></li>
				                                <li><div>游戏记录</div></li>
				                                <li><div>财务记录</div></li>
				                                <li><div>公告中心</div></li>
				                            </ul>
				                        </nav>
				                    </div>
				                    <div className="mobile-settings">
				                        <ul className="nav">
				                            <li>
				                                <div className="dropdown mobile-top-dropdown">
				                                    <div className="dropdown-toggle" id="currency" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				                                        语言
				                                        <i className="fa fa-angle-down"></i>
				                                    </div>
				                                    <div className="dropdown-menu" aria-labelledby="currency">
				                                        <div className="dropdown-item" >简体中文</div>
				                                        <div className="dropdown-item" >繁體中文</div>
				                                        <div className="dropdown-item" >English</div>
				                                    </div>
				                                </div>
				                            </li>
				                        </ul>
				                    </div>
				                    <div className="offcanvas-widget-area">
				                        <div className="off-canvas-contact-widget">
				                            <ul>
				                                <li><i className="fa fa-mobile"></i>
				                                    <div>+886 938 888888</div>
				                                </li>
				                                <li><i className="fa fa-envelope-o"></i>
				                                    <div>lexisbetman@gmail.com</div>
				                                </li>
				                            </ul>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </aside>
				    </div>
				);				
			default:
				break;	
		}
	}		
	render(){
		return(
			<div>{this.renderNav(this.props.category)}</div>
		);
	}
}


export {Navigation};