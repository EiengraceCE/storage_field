			return (
				<div>
			        <div className="breadcrumb-area breadcrumb-img bg-img eventBG">
			            <div className="container">
			                <div className="row">
			                    <div className="col-12">
			                        <div className="breadcrumb-wrap">
			                            <nav aria-label="breadcrumb">
			                                <h3 className="breadcrumb-title">Promotions</h3>
			                                <ul className="breadcrumb justify-content-center">
			                                    <li className="breadcrumb-item"><a href="index.html"><i className="fa fa-home"></i></a></li>
			                                    <li className="breadcrumb-item active" aria-current="page">优惠活动</li>
			                                </ul>
			                            </nav>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			        <div className="container">
			            <div className="row">
			                <div className="col-md-11 hm">
			                    <div className=" blog-post-item news">
			                        <div className="col-lg-12">
			                            <div className="product-review-info">
			                                <ul className="nav review-tab">
			                                    <li>
			                                        <div className="active">全部优惠</div>
			                                    </li>
			                                    <li>
			                                        <div>存款优惠</div>
			                                    </li>
			                                </ul>
			                                <div className="tab-content reviews-tab news">
			                                    <div className="tab-pane fade show active" id="tab_one">
			                                        <div className="product-carousel-3_1 slick-row-5 slick-arrow-style">
			                                            <div className="product-item">
			                                                <div className="product-thumb">
			                                                    <div><img src="img/HotNews/HotNews_001.jpg" alt="product thumb"/></div>
			                                                    <div className="button-group">
			                                                        <div title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
			                                                    </div>
			                                                </div>
			                                                <div className="product-content">
			                                                    <div className="product-caption">
			                                                        <h5 className="product-name">
			                                                            <div>红包雨下不停 高额红包会员独享</div>
			                                                        </h5>
			                                                        <div className="price-box">
			                                                            <span className="price-regular">存款抢红包</span>
			                                                        </div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                            <div className="product-item">
			                                                <div className="product-thumb">
			                                                    <div><img src="img/HotNews/HotNews_002.jpg" alt="product thumb"/></div>
			                                                    <div className="button-group">
			                                                        <div title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
			                                                    </div>
			                                                </div>
			                                                <div className="product-content">
			                                                    <div className="product-caption">
			                                                        <h6 className="product-name">
			                                                            <div>天天返水红利高达30%无上限!</div>
			                                                        </h6>
			                                                        <div className="price-box">
			                                                            <span className="price-regular">天天玩，天天送回馈</span>
			                                                        </div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                            <div className="product-item">
			                                                <div className="product-thumb">
			                                                    <div><img src="img/HotNews/HotNews_003.jpg" alt="product thumb"/></div>
			                                                    <div className="button-group">
			                                                        <div title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
			                                                    </div>
			                                                </div>
			                                                <div className="product-content">
			                                                    <div className="product-caption">
			                                                        <h6 className="product-name">
			                                                            <div>天天返水红利高达30%无上限!</div>
			                                                        </h6>
			                                                        <div className="price-box">
			                                                            <span className="price-regular">天天玩，天天送回馈</span>
			                                                        </div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                            <div className="product-item">
			                                                <div className="product-thumb">
			                                                    <div><img src="img/HotNews/HotNews_004.jpg" alt="product thumb"/></div>
			                                                    <div className="button-group">
			                                                        <div title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
			                                                    </div>
			                                                    <div className="product-label">
			                                                        <span>new</span>
			                                                    </div>
			                                                </div>
			                                                <div className="product-content">
			                                                    <div className="product-caption">
			                                                        <h6 className="product-name">
			                                                            <div>新会员注册 迎星礼拿不停</div>
			                                                        </h6>
			                                                        <div className="price-box">
			                                                            <span className="price-regular">新会员领礼金</span>
			                                                        </div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                            <div className="product-item">
			                                                <div className="product-thumb">
			                                                    <div><img src="img/HotNews/HotNews_001.jpg" alt="product thumb"/></div>
			                                                    <div className="button-group">
			                                                        <div title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
			                                                    </div>
			                                                    <div className="product-label">
			                                                        <span>new</span>
			                                                    </div>
			                                                </div>
			                                                <div className="product-content">
			                                                    <div className="product-caption">
			                                                        <h6 className="product-name">
			                                                            <div>首存送大礼 满千送千享大利</div>
			                                                        </h6>
			                                                        <div className="price-box">
			                                                            <span className="price-regular">首存大方送</span>
			                                                        </div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                    <div className="tab-pane fade" id="tab_two">
			                                        <div className="product-carousel-3_1 slick-row-5 slick-arrow-style">
			                                            <div className="product-item">
			                                                <div className="product-thumb">
			                                                    <div><img src="img/HotNews/HotNews_001.jpg" alt="product thumb"/></div>
			                                                    <div className="button-group">
			                                                        <div title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
			                                                    </div>
			                                                    <div className="product-label">
			                                                        <span>new</span>
			                                                    </div>
			                                                </div>
			                                                <div className="product-content">
			                                                    <div className="product-caption">
			                                                        <h6 className="product-name">
			                                                            <div>首存送大礼 满千送千享大利</div>
			                                                        </h6>
			                                                        <div className="price-box">
			                                                            <span className="price-regular">首存大方送</span>
			                                                        </div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>			
				</div>
			);