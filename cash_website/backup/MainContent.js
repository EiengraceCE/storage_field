import React from 'react';
import {InputItem} from './component/InputItem';
import {postRequest} from './universal_method/postRequest';

class MainContent extends React.Component{
	state={
		detectSymbol:/[-~`!@#$%^&()_+{}?;:/><,*=\s]/,
		preventAlert:false,
		currency:'RMB',
		account:'',
		register_account:'',
		register_password:'',
		password:'',
		realName:'',
		repeatPS:'',
		agent:'',			
		email:'',
		phoneNumber:'',
		QQ:'',
		WC:''	
	}
    handleChange = this.handleChange.bind(this);
    handleSubmit = this.handleSubmit.bind(this);	
    handleBlur = this.handleBlur.bind(this);		
    warning = this.warning.bind(this);
    registerFunc=this.registerFunc.bind(this);
    handleSignIn=this.handleSignIn.bind(this);		
	warning(target,message){
		alert(message);					
		target.focus();
		this.setState({
			[target.name]: this.state[target.name]
		});
		if(!this.state[target.name]){
			this.setState({
				[target.name]: ""
			});
		}
	}	
	handleChange(event){
		const target=event.target;
		if(this.state.detectSymbol.test(target.value)){
			this.warning(target,"禁止包含特殊字元");
			return;
		}
		switch (target.name){		
			case 'realName':
				if(target.value.length > 30){
					this.warning(target,"姓名不能超過30字元");
					return;
				}
				break;
			case 'register_account':
				if(target.value.length > 17){
					this.warning(target,"帳號不能超過17字元");
					return;
				}
				break;
			case 'register_password':
				if(target.value.length > 20){
					this.warning(target,"密碼不能超過20字元");
					return;
				}			
				break;
			case 'repeatPS':
				this.setState({
					preventAlert:false
				})
				if(target.value.length > 20){
					this.warning(target,"密碼不能超過20字元");
					return;
				}			
				break;
			case 'agent':	
				break;
			default:
				break;																			
		}	
		this.setState({
			[target.name]: target.value
		});				
	}
	handleBlur(event){
		const target=event.target;
		// console.log(target.name);
		switch (target.name){		
			case 'realName':
				break;
			case 'register_account':
				break;
			case 'register_password':
				break;
			case 'repeatPS':
				if(target.value!==this.state.register_password && !this.state.preventAlert){
					this.setState({
						preventAlert:false
					})
					this.warning(target,"必須與密碼一致");
					this.setState({
						[target.name]: ""
					});	

				}
				break;
			case 'agent':
				break;
			default:
				break;																			
		}
	}
	registerFunc(response){
		// console.log(response);
		if(response.errorCode===0){
			// console.log(this.state);
			this.setState({
				preventAlert:false,
				currency:'RMB',
				realName:'',
				register_account:'',
				repeatPS:'',
				register_password:'',
				agent:'',
				email:'',
				phoneNumber:'',
				QQ:'',
				WC:''
			})	
			alert("注册成功");
			console.log(response)
			var url =  'https://api-01.a45.me/api/web/Gateway.php';
	        var request = {
	            cmd: 'Login',
	            sid: response.result.sid
			};
			this.props.updateSid(response.result.sid);
			postRequest(url, request ,(response) => this.props.signInFunc(response));
		}else{
			alert("是在哈囉");
		}
	}
	handleSubmit(event){
		// console.log("Let's Submit");
		event.preventDefault();
		var url =  'https://api-01.a45.me/api/web/Gateway.php';
        var request = {
            cmd: 'MemberRegister',
			promotecode: this.state.agent,
			username: this.state.register_account,
			password: this.state.register_password,
			name: this.state.realName,
			currency: this.state.currency
		};
		postRequest(url, request ,(response) => this.registerFunc(response));		
	}	
	handleSignIn(event){
		event.preventDefault();
		var url =  'https://api-01.a45.me/api/web/Gateway.php';
		console.log(this.state.account);
		console.log(this.state.password);
        var request = {
            cmd: 'GetSid',
            user:this.state.account,
            password:this.state.password
		};		
		postRequest(url, request ,(response) => this.props.getSidFunc(response));
		this.setState({
			account:'',
			password:''
		});		
	}		
	render(){
		// console.log("MainContent render");
		switch(this.props.category){
			case "Home":
				// console.log("HomeContent");
				return (
					<div>
						{/*↓top AD area - Start*/}
						<section className="hero-slider">
							<div className="hero-slider-active slick-arrow-style slick-arrow-style_hero slick-dot-style">
								<div className="hero-single-slide hero-overlay">
									<div className="hero-slider-item hero-1 bg-img">
										<div className="container">
											<div className="row">
												<div className="col-md-12">
													<div className="hero-slider-content slide-1">
														<h1 className="slide-title">New WM</h1>
														<h2 className="slide-subtitle">WM 真人娱乐 <span>就是要你乐</span></h2>
														<div className="btn btn-large btn-bg">Play Now</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						{/*↑top AD area - End*/}
				
				        <section className="product-gallery section-padding">
				            <div className="container">
				                <div className="row">
				                    <div className="col-12">
				                        <div className="section-title text-center">
				                            <h3 className="title">真实的视觉和听觉体验</h3>
				                        </div>
				                    </div>
				                </div>
				                <div className="row">
				                    <div className="col-12">
				                        <div className="product-container">
				                            <div className="product-tab-menu">
				                                <ul className="nav justify-content-center">
				                                    <li><div className="active" data-toggle="tab">真人娱乐</div></li>
				                                    <li><div data-toggle="tab">体育博彩</div></li>
				                                    <li><div data-toggle="tab">彩票游戏</div></li>
				                                </ul>
				                            </div>
				                            <div className="tab-content">
				                                <div className="tab-pane fade show active" id="tab1">
				                                    <div className="row">
														<div className="col-lg-4 col-sm-12">
				                                        <div className="product-item">
				                                            <div className="product-thumb">
				                                                <div><img src="img/homegames/home-cate-game-01.jpg" alt=""/></div>
				                                                <div className="button-group">
				                                                    <div data-toggle="tooltip" title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
				                                                </div>
				                                                <div className="product-label">
				                                                    <span>new</span>
				                                                </div>
				                                            </div>
				                                            <div className="product-content">
				                                                <div className="product-caption">
				                                                    <h5 className="product-name">
				                                                        <div>ALLBET 真人娱乐</div>
				                                                    </h5>
				                                                    <div className="price-box">
				                                                        <span className="price-regular">最高标准私人享受</span>
				                                                    </div>
				                                                </div>
				                                            </div>
				                                        </div>	                                        
				                                    </div>
														<div className="col-lg-4 col-sm-12">
														<div className="product-item">
				                                            <div className="product-thumb">
				                                                <div><img src="img/homegames/home-cate-game-02.jpg" alt=""/></div>
				                                                <div className="button-group">
				                                                    <div data-toggle="tooltip" title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
				                                                </div>
				                                                <div className="product-label">
				                                                    <span>new</span>
				                                                </div>
				                                            </div>
				                                            <div className="product-content">
				                                                <div className="product-caption">
				                                                    <h6 className="product-name">
				                                                        <div >SB 真人娱乐</div>
				                                                    </h6>
				                                                    <div className="price-box">
				                                                        <span className="price-regular">流畅视讯体验</span>
				                                                    </div>
				                                                </div>
				                                            </div>
				                                        </div>
													</div>
														<div className="col-lg-4 col-sm-12">
														<div className="product-item">
				                                            <div className="product-thumb">
				                                                <div><img src="img/homegames/home-cate-game-03.jpg" alt=""/></div>
				                                                <div className="button-group">
				                                                    <div data-toggle="tooltip" title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
				                                                </div>
				                                                <div className="product-label">
				                                                    <span>new</span>
				                                                </div>
				                                            </div>
				                                            <div className="product-content">
				                                                <div className="product-caption">
				                                                    <h6 className="product-name">
				                                                        <div >AG 真人娱乐</div>
				                                                    </h6>
				                                                    <div className="price-box">
				                                                        <span className="price-regular">极致玩法 最高享乐</span>
				                                                    </div>
				                                                </div>
				                                            </div>
				                                        </div>
													</div>
													</div>
												</div>
												<div className="tab-pane fade" id="tab2">
				                                    <div className="row">
														<div className="col-lg-4 col-sm-12">
				                                        <div className="product-item">
				                                            <div className="product-thumb">
				                                                <div><img src="img/homegames/home-cate-game-01.jpg" alt=""/></div>
				                                                <div className="button-group">
				                                                    <div data-toggle="tooltip" title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
				                                                </div>
				                                                <div className="product-label">
				                                                    <span>new</span>
				                                                </div>
				                                            </div>
				                                            <div className="product-content">
				                                                <div className="product-caption">
				                                                    <h5 className="product-name">
				                                                        <div>UGAMING 体育博彩</div>
				                                                    </h5>
				                                                    <div className="price-box">
				                                                        <span className="price-regular">最高标准私人享受</span>
				                                                    </div>
				                                                </div>
				                                            </div>
				                                        </div>	                                        
				                                    </div>
														<div className="col-lg-4 col-sm-12">
														<div className="product-item">
				                                            <div className="product-thumb">
				                                                <div><img src="img/homegames/home-cate-game-02.jpg" alt=""/></div>
				                                                <div className="button-group">
				                                                    <div data-toggle="tooltip" title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
				                                                </div>
				                                                <div className="product-label">
				                                                    <span>new</span>
				                                                </div>
				                                            </div>
				                                            <div className="product-content">
				                                                <div className="product-caption">
				                                                    <h6 className="product-name">
				                                                        <div>SABA 体育博彩</div>
				                                                    </h6>
				                                                    <div className="price-box">
				                                                        <span className="price-regular">流畅视讯体验</span>
				                                                    </div>
				                                                </div>
				                                            </div>
				                                        </div>
													</div>
														<div className="col-lg-4 col-sm-12">
														<div className="product-item">
				                                            <div className="product-thumb">
				                                                <div><img src="img/homegames/home-cate-game-03.jpg" alt=""/></div>
				                                                <div className="button-group">
				                                                    <div data-toggle="tooltip" title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
				                                                </div>
				                                                <div className="product-label">
				                                                    <span>new</span>
				                                                </div>
				                                            </div>
				                                            <div className="product-content">
				                                                <div className="product-caption">
				                                                    <h6 className="product-name">
				                                                        <div>SABA 体育博彩</div>
				                                                    </h6>
				                                                    <div className="price-box">
				                                                        <span className="price-regular">极致玩法 最高享乐</span>
				                                                    </div>
				                                                </div>
				                                            </div>
				                                        </div>
													</div>
													</div>
												</div>
												<div className="tab-pane fade" id="tab3">
				                                    <div className="row">
														<div className="col-lg-4 col-sm-12">
				                                        <div className="product-item">
				                                            <div className="product-thumb">
				                                                <div><img src="img/homegames/home-cate-game-01.jpg" alt=""/></div>
				                                                <div className="button-group">
				                                                    <div data-toggle="tooltip" title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
				                                                </div>
				                                                <div className="product-label">
				                                                    <span>new</span>
				                                                </div>
				                                            </div>
				                                            <div className="product-content">
				                                                <div className="product-caption">
				                                                    <h5 className="product-name">
				                                                        <div>VR 彩票游戏</div>
				                                                    </h5>
				                                                    <div className="price-box">
				                                                        <span className="price-regular">最高标准私人享受</span>
				                                                    </div>
				                                                </div>
				                                            </div>
				                                        </div>	                                        
				                                    </div>
														<div className="col-lg-4 col-sm-12">
														<div className="product-item">
				                                            <div className="product-thumb">
				                                                <div><img src="img/homegames/home-cate-game-02.jpg" alt=""/></div>
				                                                <div className="button-group">
				                                                    <div data-toggle="tooltip" title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
				                                                </div>
				                                                <div className="product-label">
				                                                    <span>new</span>
				                                                </div>
				                                            </div>
				                                            <div className="product-content">
				                                                <div className="product-caption">
				                                                    <h6 className="product-name">
				                                                        <div>IG 彩票游戏</div>
				                                                    </h6>
				                                                    <div className="price-box">
				                                                        <span className="price-regular">流畅视讯体验</span>
				                                                    </div>
				                                                </div>
				                                            </div>
				                                        </div>
													</div>
														<div className="col-lg-4 col-sm-12">
														<div className="product-item">
				                                            <div className="product-thumb">
				                                                <div><img src="img/homegames/home-cate-game-03.jpg" alt=""/></div>
				                                                <div className="button-group">
				                                                    <div data-toggle="tooltip" title="PLAY NOW"><i className="fa fa-gamepad"></i></div>
				                                                </div>
				                                                <div className="product-label">
				                                                    <span>new</span>
				                                                </div>
				                                            </div>
				                                            <div className="product-content">
				                                                <div className="product-caption">
				                                                    <h6 className="product-name">
				                                                        <div>NY 彩票游戏</div>
				                                                    </h6>
				                                                    <div className="price-box">
				                                                        <span className="price-regular">极致玩法 最高享乐</span>
				                                                    </div>
				                                                </div>
				                                            </div>
				                                        </div>
													</div>
													</div>
												</div>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				                <div className="row mtn-30">
				                    <div className="col-lg-4 col-sm-6">
				                        <div className="footer-widget-item mt-30 service-time-box home-box">
				                            <h6 className="widget-title">强势服务</h6>
				                            <ul className="usefull-links area1">
				                                <li>
				                                    <div className="time-item">
				                                        <div className="time-text">
				                                            <div className="fl">
				                                                <h5>存款火速到账</h5>平均时间
				                                            </div>
				                                            <div className="fr">
				                                                <strong id="sceValu">23</strong> 秒
				                                            </div>
				                                        </div>
				                                        <div className="bar">
				                                            <i style={{width: "45%"}}></i>
				                                        </div>
				                                    </div>
				                                </li>
				                                <li>
				                                    <div className="time-item">
				                                        <div className="time-text">
				                                            <div className="fl">
				                                                <h5>取款火速到账</h5>平均时间
				                                            </div>
				                                            <div className="fr thpoint">
				                                                <strong id="sceValu2">200</strong> 秒
				                                            </div>
				                                        </div>
				                                        <div className="bar">
				                                            <i style={{width: "72%"}}></i>
				                                        </div>
				                                    </div>
				                                </li>
				                            </ul>
				                        </div>
				                    </div>
				                    <div className="col-lg-4 col-sm-6">
				                        <div className="footer-widget-item mt-30">
				                            <h6 className="widget-title">最新优惠</h6>
				                            <ul className="usefull-links area2">
				                                <li><div>VIP DOUBLE DRAGON CLUB<p>BENEFITS AS OUR VIP (LEVEL BONUS ,BIRTHDAY BONUS ,RELOAD BANUS SPORT , REBATE SPORT ,RELOAD BONUS LIVE CASINO ,REBATE LIVE CASINO, RELOAD BONUS SLOT ,REBATE SLOT ) THE HIGHEST RELOAD BONUS AND REBATE IN INDONESIA</p></div></li>
				                                <li><div>NIKMATI INTERNET BANIKING GET 1% RELOAD BONUS TAK BATAS<p>INTERNET BANKING RELOAD AND GET 1% RELOAD BONUS FOR EVERY MEMBER DOUBLE DRAGON</p></div></li>
				                            </ul>
				                        </div>
				                    </div>
				                    <div className="col-lg-4 col-sm-6">
				                        <div className="footer-widget-item mt-30">
				                            <h6 className="widget-title">帮助中心</h6>
				                            <ul className="usefull-links area3">
				                                <li><div>常见问题</div></li>
				                                <li><div>存款帮助</div></li>
				                                <li><div>取款帮助</div></li>
				                                <li><div>联系我们</div></li>
				                            </ul>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </section>				
					</div>
				);
			case "Register":
				// console.log("registerContent");
				return (
					<div>
				        <div className="breadcrumb-area breadcrumb-img bg-img" data-bg="img/banner/loginregister_bg.jpg">
				            <div className="container">
				                <div className="row">
				                    <div className="col-12">
				                        <div className="breadcrumb-wrap">
				                            <nav aria-label="breadcrumb">
				                                <h3 className="breadcrumb-title">LOGIN REGISTER</h3>
				                                <ul className="breadcrumb justify-content-center">
				                                    <li className="breadcrumb-item"><div><i className="fa fa-home"></i></div></li>
				                                    <li className="breadcrumb-item active" aria-current="page">登录 & 免费注册</li>
				                                </ul>
				                            </nav>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				        <div className="login-register-wrapper section-padding">
				            <div className="container">
				                <div className="member-area-from-wrap">
				                    <div className="row">
				                        <div className="col-lg-6">
				                            <div className="login-reg-form-wrap">
				                                <h4>登录 / Sign In</h4>
		                                        <form onSubmit={this.handleSignIn}>
						                                {InputItem("text","请输入信箱或会员账号","account",this.state.account,(event) => this.handleChange(event),true)}
						                                {InputItem("password","请输入密码","password",this.state.password,(event) => this.handleChange(event),true)}			                                        						                                		                                                        
		                                                <div className="single-input-item">
		                                                    <div className="login-reg-form-meta d-flex align-items-center justify-content-between">
		                                                        <div className="remember-meta">
		                                                            <div className="custom-control custom-checkbox">
		                                                            	{InputItem("checkbox",undefined,undefined,undefined,undefined,undefined,undefined,'記住我')}
		                                                            </div>
		                                                        </div>
		                                                        <span className="forget-pwd">忘记密码?</span>
		                                                    </div>
		                                                </div>
		                                                <div className="single-input-item">
		                                                    <button type="submit" className="btn btn-sqr">登录</button>
		                                                </div>
		                                        </form>		                                
				                            </div>
				                        </div>
				                        <div className="col-lg-6">
				                            <div className="login-reg-form-wrap sign-up-form">
				                                <h4>免费注册 / Sign Up</h4>
				                                <span className="psTred">有"*"&nbsp;号为必填项目</span>
				                                <form onSubmit={this.handleSubmit}>
				                                	{InputItem("text","* 请输入真实姓名","realName",this.state.realName,(event) => this.handleChange(event),true,undefined,"必须与您的银行帐户一致，否则无法进行取款")}	
				                                	{InputItem("text","* 请输入账号","register_account",this.state.register_account,(event) => this.handleChange(event),true,undefined,"需为5~17个英文或数字的组合")}	                               			                                	
				                                    <div className="row">
				                                        <div className="col-lg-6">
				                                        	{InputItem("password","* 请输入密码","register_password",this.state.register_password,(event) => this.handleChange(event),true,undefined,"需为6~20个英文与数字的组合")}  
				                                        </div>
				                                        <div className="col-lg-6">
				                                        	{InputItem("password","* 请再次输入密码","repeatPS",this.state.repeatPS,(event) => this.handleChange(event),true,(event) => this.handleBlur(event),undefined)}   
				                                        </div>
				                                    </div>
													<select name='currency' value={this.state.value} onChange={this.handleChange}>
													  <option value="RMB">RMB</option>
													  <option value="USD">USD</option>
													  <option value="NTD">NTD</option>
													  <option value="THB">THB</option>
													  <option value="MYR">MYR</option>
													</select>
				                                    {InputItem("text","请输入代理账号","agent",this.state.agent,(event) => this.handleChange(event),undefined,undefined,"若有代理，请输入其ID")}                                    
				                                    {InputItem("email","请输入信箱","email",this.state.email,(event) => this.handleChange(event),undefined,undefined,undefined)}                                    
				                                    {InputItem("text","请输入手机号码","phoneNumber",this.state.phoneNumber,(event) => this.handleChange(event),undefined,undefined,undefined)}                                    		                                		                                			                                    
				                                    <div className="row">
				                                        <div className="col-lg-6">
				                                		    {InputItem("password","请输入QQ","QQ",this.state.QQ,(event) => this.handleChange(event),undefined,undefined,undefined)}                                                                            
				                                        </div>
				                                        <div className="col-lg-6">
				                                		    {InputItem("password","请输入微信","WC",this.state.WC,(event) => this.handleChange(event),undefined,undefined,undefined)}                                                                            				                                        				                                			                                        
				                                        </div>
				                                    </div>
				                                    <div className="single-input-item">
				                                        <div className="login-reg-form-meta">
				                                            <div className="remember-meta">		                                				                                            	
				                                                <div className="custom-control custom-checkbox">
				                                				    {InputItem("checkbox",undefined,undefined,undefined,undefined,true,undefined,"我已屆滿合法博彩年齡，且同意各項\"用戶協議\"")}                                                                            				                                        				                                			                                        				                                                		                                                
				                                                </div>
				                                            </div>
				                                        </div>
				                                    </div>
				                                    <div className="single-input-item">
				                                        <button type="submit" className="btn btn-sqr">免费注册</button>
				                                    </div>
				                                </form>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>				
					</div>
				);
			case "Account":
				// console.log("Account");
				return (
			        <div className="checkout-page-wrapper section-padding accStyle">
			            <div className="container">
			                <div className="row">
			                    <div className="col-lg-7">
			                        <div className="checkout-billing-details-wrap">
			                            <h5 className="checkout-title">
			                                个人资料<button title="" className="nrc-button">修改 登录 / 取款 密码</button>
			                            </h5>
			                            <div className="billing-form-wrap">
			                                <div className="resume-details">
			                                    <div className="resume-item">
			                                        <p className="title">注册时间：</p>
			                                        <p className="value">{this.props.accountInfo.registerTime}</p>
			                                    </div>
			                                    <div className="resume-item">
			                                        <p className="title">上次登录时间：</p>
			                                        <p className="value">{this.props.accountInfo.lastLogin}</p>
			                                    </div>
			                                    <div className="resume-item">
			                                        <p className="title">会员账号：</p>
			                                        <p className="value">{this.props.accountInfo.account}</p>
			                                    </div>
			                                    <div className="resume-item">
			                                        <p className="title">真实姓名：</p>
			                                        <p className="value">{this.props.accountInfo.username}</p>
			                                    </div>
			                                    <div className="resume-item">
			                                        <p className="title">手机号码：</p>
			                                        <p className="value">{this.props.accountInfo.phone}</p>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                        <div className="checkout-billing-details-wrap">
			                            <h5 className="checkout-title">优惠信息</h5>
			                            <div className="billing-form-wrap">
			                               <div className="row">
			                                    <div className="col-12">
			                                        <div className="checkoutaccordion" id="checkOutAccordion">
			                                            <div className="card">
			                                                <h6>存款抢红包，红包雨下不停，高额红包会员独抢!!
			                                                    <span data-toggle="collapse" data-target="#logInaccordion">详细内容</span>
			                                                </h6>
			                                                <div id="logInaccordion" className="collapse" data-parent="#checkOutAccordion">
			                                                    <div className="card-body">
			                                                        <p>即日起，抢红包活动全面升级！凡是存款100元以上的会员皆可参加现金抢红包活动，点选活动中 "进行抢红包"参与抢红包，单个红包最高8888元，快来试试您的运气吧！更多给力现金回馈活动筹备中，敬请关注。</p>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                            <div className="card">
			                                                <h6>存款抢红包，红包雨下不停，高额红包会员独抢!!
			                                                    <span data-toggle="collapse" data-target="#logInaccordion2">详细内容</span>
			                                                </h6>
			                                                <div id="logInaccordion2" className="collapse" data-parent="#checkOutAccordion">
			                                                    <div className="card-body">
			                                                        <p>即日起，抢红包活动全面升级！凡是存款100元以上的会员皆可参加现金抢红包活动，点选活动中 "进行抢红包"参与抢红包，单个红包最高8888元，快来试试您的运气吧！更多给力现金回馈活动筹备中，敬请关注。</p>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                    <div className="col-lg-5">
			                        <div className="order-summary-details">
			                            <div className="order-summary-content">
			                                <div>
			                                    <div className="box bank-card">
			                                        <div className="title">银行卡资料</div>
			                                        <div className="add-info">
			                                            <i className="fa fa-plus-circle"></i>
			                                            <div>
			                                                <div className="add-info-title">新增银行卡</div>
			                                                <div className="add-info-dec">请先新增银行卡资料</div>
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                <div className="wallets box">
			                                    <div className="title">
			                                        <div>所有钱包</div>
			                                        <div className="currency"></div>
			                                    </div>
			                                    <div className="price">{this.props.accountInfo.balance}</div>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
				);				
			default:
				break;	
		}		
	}
}

export {MainContent};