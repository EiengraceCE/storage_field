import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import axios from 'axios';
import './dist/css/adminlte.min.css';
import './plugins/fontawesome-free/css/all.min.css';
import './plugins/icheck-bootstrap/icheck-bootstrap.min.css';
import {BrowserRouter, Route, Redirect, Switch,browserHistory} from 'react-router-dom';
import {Router} from 'react-router';
import Login from "./Login";
import Board from "./Board";
import { createBrowserHistory } from 'history';
const history = createBrowserHistory();
class Ares extends React.Component {
    constructor(props) {
        super(props);
        console.log("reconstructor");
        this.state={
            currentPage:"login",
            currentMainContent:"empty",
            currentNav:"Classic",
            account:'moore_huang@you-ce.net',
            email:'',
            password:'abc123'	,
            sid:'',
            startAt:new Date('1970-01-01 12:00:00'),
            endAt:new Date(),
            access_token:'',
            memberPage:1,
            memberData:[],
            nowPick:'',
        };
    }
    handleChange(event){//每一鍵輸入後觸發 檢查輸入字元
        const target=event.target;
        this.setState({
            [target.name]: target.value
        });
    }
    submitLogin(event){
        event.preventDefault();
        //------------------------------------------------------
        axios.post('/api/login',
            {
                email:this.state.account,
                password:this.state.password
            },
            {
                baseURL:'http://www.localhost.ares.net'
            })
            .then((result) => this.getSidFunc(result.data))
            .catch((error) => { console.error(error) })
    }
    submitLogin2(event){
        event.preventDefault();
        this.setState({
            sid:'Im in',
            currentPage:'home',
        });
    }
    getSidFunc(response){
        if(response.status===1){
            console.log("login success");
            history.push("/dashboard",true);
            console.log(history);
            this.setState({
                sid:'Im in',
                currentPage:'home',
                access_token:response.result.access_token
            });
            console.log("----------------------------");

        }else{
            console.log("login fail");
            alert("wrong account or password");
        }
    }


    render() {
        console.log("////////////");
        console.log(this.state);
        return (
            <Router history={history}>
                <Switch>
                    <Route exact path="/">
                       <Login
                            submitLogin={(event) => this.submitLogin(event)}
                            submitLogin2={(event) => this.submitLogin2(event)}
                            handleChange={(event) => this.handleChange(event)}
                            account={this.state.account}
                            password={this.state.password}
                        />)
                    </Route>
                    <Route exact path="/dashboard" component={Board}>

                    </Route>
                </Switch>
            </Router>
        );
    }
}
// ========================================

ReactDOM.render(
    <Ares />,
    document.getElementById('root')
);